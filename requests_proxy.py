#coding:utf-8
__author__ = 'yishuangxi'
import requests

proxies = {
    "http": "http://172.22.208.101:8080",
    "https": "http://172.22.208.102:8080",
}


#def get(url, headers=None):
#    r = requests.get(url, proxies=proxies, headers=headers)
#    return r

#def post(url, headers=None):
#    r = requests.post(url, proxies=proxies, headers=headers)
#    return r


#线下测试无需使用代理
# def get(url, headers=None):
#     r = requests.get(url, proxies=proxies, headers=headers)
#     return r

#线下测试无需使用代理
def get(url, headers=None):
    r = requests.get(url, headers=headers)
    return r

#
def post(url, headers=None):
    r = requests.post(url, headers=headers)
    return r
