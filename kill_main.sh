#!/bin/sh

pids=$(ps -ef|grep main.py|awk '{print $2}' | tr "\n" " ")

$(kill -9 $pids)