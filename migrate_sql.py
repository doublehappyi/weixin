#coding:utf-8
__author__ = 'yishuangxi'
import MySQLdb
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import urlparse

conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db,charset="utf8")
cursor = conn.cursor()

cursor.execute('select * from article_1010')
rows = cursor.fetchall()
for article_row in rows:
    article_url = article_row[2]
    try:
        biz = urlparse.parse_qs(urlparse.urlparse(article_url).query)['__biz'][0]
    except Exception as e:
        # logger.debug('get __biz error: {0}'.format(e))
        continue
    # logger.debug('article_url:{0}, biz: {1}'.format(article_url, biz))

    cursor.execute('select article_url from article where article_url="{0}"'.format(article_url))
    r = cursor.fetchone()
    if r is not None:
        continue

    cursor.execute('select wx_id from weixin where wx_biz="{0}"'.format(biz))
    wx_row = cursor.fetchone()
    if wx_row is None:
        continue

    wx_id =wx_row[0]
    # logger.debug('wx_id: {0}'.format(wx_id))
    # logger.debug('{0}, {1}, {2}, {3}, {4}, {5}, {6}'.format(article_row[1],article_row[2],article_row[3],article_row[4],article_row[5],article_row[6], wx_id))
    sql = 'insert into article values (null, "{0}", "{1}", {2}, {3}, {4}, {5}, {6})'\
                   .format(article_row[1],article_row[2],article_row[3],article_row[4],article_row[5],article_row[6], wx_id)
    logger.debug('sql: '+sql)
    try:
        cursor.execute(sql)
        conn.commit()
    except Exception as e:
        # logger.error('Exception: {0}'.format(e))
        continue