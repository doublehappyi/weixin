#创建文章信息表

CREATE DATABASE IF NOT EXISTS wx DEFAULT CHARACTER SET utf8 ;
use wx;
#创建微信账号信息表
CREATE TABLE IF NOT EXISTS weixin(
  wx_id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  wx_name VARCHAR(20) NOT NULL ,
  wx_account VARCHAR(20) UNIQUE NOT NULL ,
  wx_biz VARCHAR(16) DEFAULT NULL ,#这里要把wx_biz的unique属性去掉，因为有些账号的wx_biz在搜狗里面搜索不到，但是又需要标记已经查找过了。
  wx_intro TEXT DEFAULT NULL ,#微信介绍
  wx_qrcode VARCHAR(500) DEFAULT NULL,#微信二维码图片地址
  history_status TINYINT DEFAULT 0,#0代表从未获取过数据,1代表获取过第一页数据,2代表已获取过历史数据
  add_time INT(10) NOT NULL ,#添加到数据库时间
  update_time INT(10) NOT NULL #更新本条数据时间
);

#创建数据库
CREATE TABLE IF NOT EXISTS article(
  article_id INT(10) unsigned NOT NULL auto_increment PRIMARY KEY ,
  article_title VARCHAR(200) DEFAULT NULL ,
  article_url VARCHAR(500) NOT NULL ,
  article_read_num INT(10) DEFAULT NULL ,#阅读次数
  article_like_num INT(10) DEFAULT NULL ,#点赞次数
  article_publish_time INT(10) DEFAULT NULL ,#文章发表时间
  update_time INT(10) NOT NULL ,#本条数据更新时间:插入时间和抓取时间
  ref_wx_id INT(10) NOT NULL
);