# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################
import threading
import time
import MySQLdb


class StatUpdaterManager(object):
    def __init__(self, c_queue, c_thread_num):
        self.c_queue = c_queue
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(StatUpdaterWorker(self.c_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()

class StatUpdaterWorker(threading.Thread):
    def __init__(self, c_queue):
        super(StatUpdaterWorker, self).__init__()
        self.c_queue = c_queue

        self.setDaemon(True)
        self.start()
    def run(self):
        conn = settings.getConnection()
        cursor = conn.cursor()
        while 1:
            try:
                logger.debug('StatUpdaterWorker: get data from c_queue start')
                row = self.c_queue.get()
                self.c_queue.task_done()
                logger.debug('StatUpdaterWorker: get data from c_queue: %s' % str(row))
                #检测是否已经到了队列尽头
                if not row[0]:
                    logger.debug('StatUpdaterWorker: no more data in c_queue')
                    break

                article_id, read_num, like_num = row
                sql_str = """update %s t set t.article_read_num=%s, t.article_like_num=%s where article_id=%d""" % (settings.db['table']['article'], read_num, like_num, article_id)
                logger.debug('StatUpdaterWorker: sql_str: %s' % sql_str)
                cursor.execute(sql_str)
                conn.commit()
            except MySQLdb.OperationalError as e:
                logger.debug('StatUpdaterWorker: MySQLdb.OperationalError: %s' % e)
                break
            except Exception as e:
                logger.debug('StatUpdaterWorker: Exception: %s' % e)
                time.sleep(1)

        logger.debug('StatUpdaterWorker: close conn and cursor start')
        conn.close()
        cursor.close()
        logger.debug('StatUpdaterWorker: close conn and cursor end')




