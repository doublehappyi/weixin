# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
# ####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
# ####################日志设置#####################
import threading
import time
import stat_utils
import urlparse


class StatSpiderManager(object):
    def __init__(self, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(StatSpiderWorker(self.c_queue, self.p_queue, settings.config[i]))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('StatSpiderManager:Putting (None, ) into p_queue')
            self.p_queue.put((None,))


class StatSpiderWorker(threading.Thread):
    def __init__(self, c_queue, p_queue, config):
        super(StatSpiderWorker, self).__init__()
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.config = config
        self.conn = settings.getConnection()
        self.cursor = self.conn.cursor()
        self.setDaemon(True)
        self.start()

    def run(self):
        uin, key = None, None #初始化
        curr_biz = None
        while 1:
            logger.debug(self.getName()+'StatSpiderWorker: sleep 5 seconds')
            time.sleep(5)
            #1，首先拿article_id, article_url, ref_wx_id
            logger.debug(self.getName()+'StatSpiderWorker: get data from p_queue start')
            row = self.c_queue.get();self.c_queue.task_done()
            logger.debug(self.getName()+'StatSpiderWorker: get data from p_queue end')
            if row[0] is None:
                logger.warning(self.getName()+'StatSpiderWorker: no more data in c_queue')
                break
            logger.debug(self.getName()+'StatSpiderWorker: row ok: %s' % str(row))
            article_id, article_url, ref_wx_id = row
            if not article_id or not article_url or not ref_wx_id:
                logger.warning(self.getName()+'StatSpiderWorker: article_id or article_url or ref_wx_id is error: {0}'.format(str(row)))
                logger.debug(self.getName()+'StatSpiderWorker: stat_data failed: article_id or article_url or ref_wx_id error：article_url:{0}'.format(article_url))
                continue

            #2，再拿uin,key: 根据article_url的biz参数决定是否需要重新拉取uin
            if not curr_biz or curr_biz not in article_url:
                try:
                    curr_biz = urlparse.parse_qs(urlparse.urlparse(article_url).query)['__biz'][0]
                except KeyError as e:
                    logger.debug(self.getName()+'StatSpiderWorker: urlparse KeyError:{0}, E:{1}'.format(article_url, e))
                    continue
                except Exception as e:
                    logger.debug(self.getName()+'StatSpiderWorker: urlparse Exception:{0}, E:{1}'.format(article_url, e))
                    continue
                uin, key = None, None
            if not uin or not key:
                uin, key = stat_utils.get_uin_and_key(self.config, article_url)
            if not uin or not key:
                logger.debug(self.getName()+'StatSpiderWorker: uin, key failed：article_url:{0}'.format(article_url))
                logger.debug(self.getName()+'StatSpiderWorker: stat_data failed: uin, key error：article_url:{0}'.format(article_url))
                continue

            #3，再拿stat_url
            stat_url = self.get_stat_url(article_url, uin, key)
            if not stat_url:
                logger.warning(self.getName()+'StatSpiderWorker: stat_url failed：article_url:{0}'.format(article_url))
                logger.debug(self.getName()+'StatSpiderWorker: stat_data failed: stat_url error：article_url:{0}'.format(article_url))
                continue

            #4，再拿stat_data
            stat_data = stat_utils.request_stat_data(stat_url)
            #这里不能用not，因为有可能read_num为0，或者like_num为0
            if stat_data[0] is None:
                logger.warning(self.getName()+'StatSpiderWorker: stat_data failed：stat_data error: article_url:{0}, stat_url:{1}'.format(article_url, stat_url))
                continue

            #5，把数据放入队列
            read_num, like_num = stat_data
            logger.debug(self.getName()+'StatHandlerWorker: put (article_id, read_num, like_num) to p_queue start ({0}, {1}, {2})'.format(article_id, read_num, like_num))
            self.p_queue.put((article_id, read_num, like_num))
            logger.debug(self.getName()+'StatHandlerWorker: put (article_id, read_num, like_num) to p_queue end ({0}, {1}, {2})'.format(article_id, read_num, like_num))

        self.conn.close()
        self.cursor.close()
        logger.warning(self.getName()+'StatSpiderWorker: conn and cursor closed !')

    def get_stat_url(self, article_url, uin, key):
        stat_url = None
        try:
            base_url = 'http://mp.weixin.qq.com/mp/getappmsgext?'
            path_url = article_url.split('?')[1].split('#')[0] + '&uin=' + uin + '&key=' + key
            stat_url = base_url + path_url
        except Exception as e:
            stat_url = None
            logger.debug(self.getName()+'StatSpiderWorker: get_stat_url Exception: {0}'.format(e))
        stat_url = stat_url.replace('&amp;', '&')
        return stat_url

