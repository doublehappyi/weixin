import requests
article_url = 'http://mp.weixin.qq.com/s?__biz=MTQzMjE1NjQwMQ==&mid=400812766&idx=4&sn=a2b4523059c59af743ee01c84f54bcfe&scene=0&key=d4b25ade3662d643885ee3d524a8e3caadb3db7890af5499d6b1c4e770504902fa8bf9ffc6406f48b25b7042bf792516&ascene=7&uin=MTcwMDEyNDEzMA%3D%3D&devicetype=android-21&version=26030532&nettype=WIFI&pass_ticket=tb%2FQ6Iseoydxlln1mx0J0GbiRK3nauLg8Tv1p6%2FaFNYSZFdLREOrD%2Bx9zOXVQl0G'
article_r = requests.get(article_url)
print article_r.headers
d = {}
setcookies = article_r.headers['Set-Cookie'].split(';')
for c in setcookies:
    if 'wxticket' in c or 'wxticketkey' in c or 'wxtokenkey' in c:
        l = c.split(',')
        for c in l:
            if 'wxticket' in c or 'wxticketkey' in c or 'wxtokenkey' in c:
                it = c.strip().split('=')
                k = it[0]
                v = it[1]
                d[k] = v
wxticket = d['wxticket']
wxticketkey = d['wxticketkey']
wxtokenkey = d['wxtokenkey']

print 'wxticket: ' + wxticket
print 'wxticketkey: ' + wxticketkey
print 'wxtokenkey: ' + wxtokenkey

stat_url = 'http://mp.weixin.qq.com/mp/getappmsgext?__biz=MTQzMjE1NjQwMQ==&mid=400812766&sn=a2b4523059c59af743ee01c84f54bcfe&idx=4&scene=0&title=%E4%BA%BF%E8%88%AA%E5%88%9A%E5%8F%91%E5%B8%83%E4%BA%86%E6%96%B0%E6%AC%BE%E6%97%A0%E4%BA%BA%E6%9C%BA%EF%BC%8C%E8%81%94%E5%90%88%E5%88%9B%E5%A7%8B%E4%BA%BA%E5%B0%B1%E5%87%BA%E8%B5%B0%E4%BA%86&ct=1446768294&devicetype=android-21&version=&f=json&r=0.5068809266667813&is_need_ad=1&comment_id=4142420975&is_need_reward=0&both_ad=1&reward_uin_count=0&uin=MTcwMDEyNDEzMA%253D%253D&key=d4b25ade3662d643885ee3d524a8e3caadb3db7890af5499d6b1c4e770504902fa8bf9ffc6406f48b25b7042bf792516&pass_ticket=tb%25252FQ6Iseoydxlln1mx0J0GbiRK3nauLg8Tv1p6%25252FaFNYSZFdLREOrD%25252Bx9zOXVQl0G&wxtoken=3096119239&devicetype=android-21&clientversion=26030532&x5=1'

stat_headers = {
    'X-Requested-With': 'XMLHttpRequest',
    'Referer': article_url,
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Accept-Charset': 'utf-8, iso-8859-1, utf-16, *;q=0.7',
    'Origin': 'http://mp.weixin.qq.com',
    'Accept': 'text/xml, text/html, application/xhtml+xml, image/png, text/plain, */*;q=0.8',
    'User-Agent': 'Mozilla/5.0 (Linux; U; Android 5.0.1; zh-cn; LG-D857 Build/LRX22G) AppleWebKit/533.1 (KHTML, like Gecko)Version/4.0 MQQBrowser/5.4 TBS/025478 Mobile Safari/533.1 MicroMessenger/6.3.5.50_r1573191.640 NetType/WIFI Language/zh_CN',
    'Accept-Language': 'zh-CN',
    'Accept-Encoding': 'gzip',
    'Connection': 'keep-alive',
    'Host': 'mp.weixin.qq.com',
    'Cookie': 'wxticket=' + wxticket + '; wxticketkey=' + wxticketkey + '; wxtokenkey=' + wxtokenkey + '; mobileUV=1_1506d9b90fa_18ef7; pgv_pvi=7849827328; pgv_pvid=9668217500',
    'Q-UA2': 'QV=2&PL=ADR&PR=TBS&PB=GE&VE=B1&VN=1.4.0.1105&CO=X5&COVN=025478&RF=PRI&PP=com.tencent.mm&PPVC=26030532&RL=1440*2392&MO= LG-D857 &DE=PHONE&OS=5.0.1&API=21&CHID=11111&LCID=9422',
    'Q-GUID': 'd1f639cc33dec6d864844b1f13b788cb',
    'Q-Auth': '31045b957cf33acf31e40be2f3e71c5217597676a9729f1b',
    'Content-Length': 0
}

text = requests.post(stat_url, headers=stat_headers).text

print text
