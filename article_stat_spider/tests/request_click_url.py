# coding:utf-8
__author__ = 'yishuangxi'
import requests
import json
import time
import random
headers = {
    "Accept": "application/json, text/plain, */*",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "zh-CN,zh;q=0.8",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive",
    "Content-Length": "533",
    "Content-Type": "application/json;charset=UTF-8",
    "Cookie": "MM_WX_NOTIFY_STATE=1; MM_WX_SOUND_STATE=1; webwxuvid=baeed78697f222e6555799b2d84984d636b29b310fe70ce93f6b03b4d452fde0b06693edb4ec9444669e390430f64c2f; lv_irt_id=8145928a8ad70c27180ef689abef6ff5; pt_clientip=55e77f000001f72b; pt_serverip=601c0aa687cc6603; qz_gdt=fsmagvqkaaapwubznq2a; qq_photo_key=34d64cf82025adea6eeda71709368fa8; uid=112784007; qm_username=2546618112; qm_sid=6cdba56e1f569b9a1a83107daea5e53d,qQktwcW1meXBxWklSQ0czNUZWRmJXaWd5d3F0NTZlMEVWTWJXUUtma2VMQV8.; pgv_pvi=5216305152; pgv_si=s2372751360; mm_lang=zh_CN; wxloadtime=1443145045_expired; RK=lPPTLizJk8; aboutVideo_v=0; pt2gguin=o2546618112; uin=o2546618112; skey=@lFeFN3Vc9; ptisp=ctc; ptcz=62136008d826b10c4982b7584bb55152809e04af5fb14e774f51a5994c2d4691; o_cookie=2546618112; pgv_info=ssid=s5618897078; pgv_pvid=317227288; dc_vplaying=0; wxpluginkey=1443143308; wxuin=1700124130; wxsid=QTIwYJIG7bli57ek; webwx_data_ticket=AQYA4OjpmbtgCJrMppjaxCQu",
    "Host": "wx2.qq.com",
    "Origin": "https://wx2.qq.com",
    "Pragma": "no-cache",
    "Referer": "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxindex?t=v2",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36"
}

# data = {"BaseRequest":{"Uin":1700124130,"Sid":"QTIwYJIG7bli57ek","Skey":"@crypt_9fd05067_1e607fe2c2d0e26c82571ab8b165c2d4","DeviceID":"e682418413925916"},"Msg":{"Type":1,"Content":"http://mp.weixin.qq.com/s?__biz=MjM5MDAxOTczNQ==&mid=208083026&idx=4&sn=fe5f229e04295514837eb0c740b0103a&scene=4#wechat_redirect","FromUserName":"@b5527bb34bef5945004b28e36a74fdb4ca3ecfe2d1d491056561e463b249a95e","ToUserName":"@8e452dab2583c736968f184af3cf065f577712cac191c0d8bf54479dbda6dcf3","LocalID":"14431631274110486","ClientMsgId":"14431631274110486"}}
# data = {"BaseRequest":{"Uin":1700124130,"Sid":"QTIwYJIG7bli57ek","Skey":"@crypt_9fd05067_1e607fe2c2d0e26c82571ab8b165c2d4","DeviceID":"e196618823567405"},"Msg":{"Type":1,"Content":"http://mp.weixin.qq.com/s?__biz=MjM5MDAxOTczNQ==&mid=207982720&idx=4&sn=e93eacf90e34a3b47e0d753e1a643d6f&scene=4#wechat_redirect","FromUserName":"@b5527bb34bef5945004b28e36a74fdb4ca3ecfe2d1d491056561e463b249a95e","ToUserName":"@8e452dab2583c736968f184af3cf065f577712cac191c0d8bf54479dbda6dcf3","LocalID":"14431639043810759","ClientMsgId":"14431639043810759"}}
# data = {"BaseRequest":{"Uin":1700124130,"Sid":"QTIwYJIG7bli57ek","Skey":"@crypt_9fd05067_1e607fe2c2d0e26c82571ab8b165c2d4","DeviceID":"e664717334089801"},"Msg":{"Type":1,"Content":"http://mp.weixin.qq.com/s?__biz=MjM5MDAxOTczNQ==&mid=207728394&idx=3&sn=281adc6a677866fb104e2f9f104b0698&scene=4#wechat_redirect","FromUserName":"@b5527bb34bef5945004b28e36a74fdb4ca3ecfe2d1d491056561e463b249a95e","ToUserName":"@8e452dab2583c736968f184af3cf065f577712cac191c0d8bf54479dbda6dcf3","LocalID":"14431639749910428","ClientMsgId":"14431639749910428"}}
# data = {"BaseRequest":{"Uin":1700124130,"Sid":"QTIwYJIG7bli57ek","Skey":"@crypt_9fd05067_1e607fe2c2d0e26c82571ab8b165c2d4","DeviceID":"e206895727431402"},"Msg":{"Type":1,"Content":"http://mp.weixin.qq.com/s?__biz=MjM5MDAxOTczNQ==&mid=207684434&idx=2&sn=d1e20f48add4868365db5bac5c413404&scene=4#wechat_redirect","FromUserName":"@b5527bb34bef5945004b28e36a74fdb4ca3ecfe2d1d491056561e463b249a95e","ToUserName":"@8e452dab2583c736968f184af3cf065f577712cac191c0d8bf54479dbda6dcf3","LocalID":"14431643371010147","ClientMsgId":"14431643371010147"}}
ClientMsgId = str(int(time.time())*1000) + str(random.randint(1000, 9999))
LocalID = ClientMsgId
print 'LocalID: '+LocalID
data = {"BaseRequest":{"Uin":1700124130,"Sid":"QTIwYJIG7bli57ek","Skey":"@crypt_9fd05067_1e607fe2c2d0e26c82571ab8b165c2d4","DeviceID":"e206895727431402"},"Msg":{"Type":1,"Content":"http://mp.weixin.qq.com/s?__biz=MjM5MDAxOTczNQ==&mid=207684434&idx=2&sn=d1e20f48add4868365db5bac5c413404&scene=4#wechat_redirect","FromUserName":"@b5527bb34bef5945004b28e36a74fdb4ca3ecfe2d1d491056561e463b249a95e","ToUserName":"@8e452dab2583c736968f184af3cf065f577712cac191c0d8bf54479dbda6dcf3","LocalID":LocalID,"ClientMsgId":ClientMsgId}}
body = json.dumps(data)
r = requests.post('https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxsendmsg', headers=headers,data=body)
print r.status_code
print r.text
