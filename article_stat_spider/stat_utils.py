# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
import requests_proxy
# ####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
# ####################日志设置#####################

import time
import urlparse
import requests
import json
import urllib


def open_new_tab(driver):
    driver.execute_script("window.open('');")
    #sleep 2秒等待窗口打开动作执行完毕
    time.sleep(2)

def isSessionReady(driver):
    logger.debug('Please open your session window')
    try:
        title_name = driver.find_element_by_css_selector('a.title_name').text
        if title_name:
            return True
        else:
            return False
    except Exception as e:
        logger.warning('stat_utils: open_new_tab: Exception: %s' % e)
        return False

def get_uin_and_key(config, article_url):
    uin_key_url = config['base_url']+'/cgi-bin/mmwebwx-bin/webwxcheckurl?requrl=' + urllib.quote(article_url) + '&'+ config['key']
    logger.debug('stat_utils: get_uin_and_key: uin_key_url:{0}'.format(uin_key_url))
    uin, key = None, None
    try:
        headers = {
            'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Upgrade-Insecure-Requests':1,
            'Cookie':config['cookie'],
            'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36'
        }
        r = requests_proxy.get(uin_key_url, headers=headers)
        current_url = r.url
        logger.debug('stat_utils: get_uin_and_key:current_url: {0}'.format(current_url))
        key = urlparse.parse_qs(urlparse.urlparse(current_url).query)['key'][0]
        uin = urlparse.parse_qs(urlparse.urlparse(current_url).query)['uin'][0]
    except Exception as e:
        uin, key = None, None
        logger.warning('stat_utils: get_uin_and_key: Exception: (uin_key_url:%s, E:%s)' % (uin_key_url, e))

    return (uin, key)

def request_stat_data(stat_url):
    logger.debug('stat_utils: request_stat_data: stat_url:{0}'.format(stat_url))
    read_num, like_num = None, None
    headers = {
        'Accept': 'text/xml, text/html, application/xhtml+xml, image/png, text/plain, */*;q=0.8',
        'Accept-Charset': 'utf-8, iso-8859-1, utf-16, *;q=0.7',
        'Accept-Encoding': 'gzip',
        'Accept-Language': 'zh-CN',
        'User-Agent': 'Mozilla/5.0 (Linux; U; Android 5.0.1; zh-cn; LG-D857 Build/LRX22G) AppleWebKit/533.1 (KHTML, like Gecko)Version/4.0 MQQBrowser/5.4 TBS/025469 Mobile Safari/533.1 MicroMessenger/6.2.5.50_rbb77fd6.621 NetType/WIFI Language/zh_CN',
        'X-Requested-With': 'XMLHttpRequest'
    }
    try:
        text = requests_proxy.post(stat_url, headers=headers).text
        logger.debug('stat_utils: request_stat_data: {0}'.format(text))
        data = json.loads(text)
        appmsgstat = data['appmsgstat']
        read_num = appmsgstat['read_num']
        like_num = appmsgstat['like_num']
    except KeyError as e:
        logger.warning('stat_utils: request_stat_data: KeyError:(stat_url: {0}, E: {1})'.format(stat_url, e))
    except requests.HTTPError as e:
        logger.warning('stat_utils: request_stat_data: requests.HTTPError:stat_url:{0}, E:{1}'.format(stat_url, e))
    except Exception as e:
        logger.warning('stat_utils: request_stat_data: Exception (stat_url:{0}, E: {1}'.format(stat_url, e))

    return (read_num, like_num)