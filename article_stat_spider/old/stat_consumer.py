# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################
import threading
import stat_utils
import requests
import json
import time


class StatConsumerManager(object):
    def __init__(self, lock, click_url_dict, c_queue, p_queue, c_thread_num, p_thread_num):
        self.lock = lock
        self.click_url_dict = click_url_dict
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(p_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(StatConsumerWorker(self.lock, self.click_url_dict, self.c_queue, self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('StatConsumerWorker:Putting (None, ) into p_queue')
            self.p_queue.put((None,))

class StatConsumerWorker(threading.Thread):
    def __init__(self, lock, click_url_dict, c_queue, p_queue):
        super(StatConsumerWorker, self).__init__()
        self.lock = lock
        self.click_url_dict = click_url_dict
        self.c_queue = c_queue
        self.p_queue = p_queue

        self.setDaemon(True)
        self.start()

    def run(self):
        row, key = None, None
        base_url = 'http://mp.weixin.qq.com/mp/getappmsgext?'
        retry = 0
        while 1:
            if row is None:
                logger.debug('StatConsumerWorker: get data from c_queue start')
                row = self.c_queue.get()
                self.c_queue.task_done()
                logger.debug('StatConsumerWorker: get data from c_queue end %s' % str(row))

            if row[0] is None:
                logger.debug('StatConsumerWorker: no more data in c_queue')
                break

            driver, article_id, article_url, ref_wx_id = row

            if key is None:
                logger.debug('StatConsumerWorker: get click_url start')
                with self.lock:
                    logger.debug('StatConsumerWorker: click_url_dict: %s' % self.click_url_dict)
                    click_url = self.click_url_dict[ref_wx_id]
                logger.debug('StatConsumerWorker: get click_url end %s' % click_url)

                logger.debug('StatConsumerWorker: get_uin_and_key start')
                uin, key = stat_utils.get_uin_and_key(self.lock, driver, click_url)
                logger.debug('StatConsumerWorker: get_uin_and_key end (%s, %s)' % (uin, key))

            path_url = article_url.split('?')[1].split('#')[0] + '&uin=' + uin+ '&key=' + key
            stat_url = base_url + path_url
            logger.debug('StatConsumerWorker: stat_url %s' % stat_url)

            logger.debug('StatConsumerWorker: read_num, like_num start')
            read_num, like_num = self.get_stat_data(stat_url)
            logger.debug('StatConsumerWorker: read_num, like_num end(%s, %s)' % (read_num, like_num))

            #如果read_num获取失败，
            if read_num is None or like_num is None:
                logger.debug('StatConsumerWorker: read_num, like_num failed (%s, %s, %s)' % (article_id, read_num, like_num))
                key = None
                retry +=1
                logger.debug('StatConsumerWorker: (%s, %s, %s), retry :%s' % (article_id, read_num, like_num, retry))
                if retry > settings.max_retry:
                    logger.debug('StatConsumerWorker')
                    retry = 0
                    row = None
            else:
                logger.debug('StatConsumerWorker: put (article_id, read_num, like_num) to p_queue start (%s, %s, %s) stat_url:%s' % (article_id, read_num, like_num, stat_url))
                self.p_queue.put((article_id, read_num, like_num))
                logger.debug('StatConsumerWorker: put (article_id, read_num, like_num) to p_queue end (%s, %s, %s)' % (article_id, read_num, like_num))
                row = None
            logger.debug('StatConsumerWorker: sleep 5 seconds')
            time.sleep(5)

    def get_stat_data(self, url):
        read_num, like_num = None, None
        headers = {
            'Accept': 'text/xml, text/html, application/xhtml+xml, image/png, text/plain, */*;q=0.8',
            'Accept-Charset': 'utf-8, iso-8859-1, utf-16, *;q=0.7',
            'Accept-Encoding': 'gzip',
            'Accept-Language': 'zh-CN',
            'User-Agent': 'Mozilla/5.0 (Linux; U; Android 5.0.1; zh-cn; LG-D857 Build/LRX22G) AppleWebKit/533.1 (KHTML, like Gecko)Version/4.0 MQQBrowser/5.4 TBS/025469 Mobile Safari/533.1 MicroMessenger/6.2.5.50_rbb77fd6.621 NetType/WIFI Language/zh_CN',
            'X-Requested-With': 'XMLHttpRequest'
        }
        retry = 0
        while 1:
            try:
                text = requests.get(url, headers=headers).text
                logger.debug('StatConsumerWorker: get_stat_data: %s' % text)
                data = json.loads(text)
                appmsgstat = data['appmsgstat']
                read_num = appmsgstat['read_num']
                like_num = appmsgstat['like_num']
                break
            except KeyError as e:
                logger.warning('StatConsumerWorker: get_stat_data: KeyError:(%s, %s)' % (url, e))
                break
            except requests.HTTPError as e:
                retry+=1
                logger.warning('StatConsumerWorker: get_stat_data: requests.HTTPError:(retry:%s, )' % (retry,url, e))
                if retry > settings.max_retry:
                    break
            except Exception as e:
                logger.warning('StatConsumerWorker: get_stat_data: Exception (url:%s, e:%s' % (url, e))
                break

        return (read_num, like_num)




