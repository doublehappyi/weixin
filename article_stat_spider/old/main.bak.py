# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import Queue
import MySQLdb


def init_wx_queue(p_thread_num):
    q = Queue.Queue()
    conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db,
                           charset="utf8")
    cursor = conn.cursor()

    select_str = """select wx_id, wx_biz from %s""" % (settings.tb_weixin)
    cursor.execute(select_str)
    rows = cursor.fetchall()
    for row in rows:
        logger.debug('main.py: init_wx_queue:put into queue (wx_id:%s, wx_biz:%s)' % row)
        q.put(row)

    for i in range(p_thread_num):
        logger.debug('main.py: init_wx_queue:put into queue (None, )')
        q.put((None,))

    return q


from stat_producer import StatProducerManager
from stat_consumer import StatConsumerManager
from stat_updater import StatUpdaterManager
import threading

if __name__ == '__main__':
    # 全局线程锁
    lock = threading.Lock()
    # 全局可点击url字典：key是wx_id，值是该key对应的某一篇文章在微信发送栏发送之后产生的https链接，
    # 该https链接在浏览器请求后可以生成用于获取文章阅读数的key值
    click_url_dict = {}
    #文章生产者，消费者以及数据库更新模块的线程数：生产者需要有界面支持，一个生产者对应一个账号
    producer_thread_num = 2
    consumer_thread_num = 2
    stat_updater_thread_num = 1

    #微信账号队列，由各生产者共享并消费
    wx_queue = init_wx_queue(producer_thread_num)
    #文章队列：个生产者往队列里面添加数据，统一由消费者消费
    article_queue = Queue.Queue(20)
    #数据库写入队列
    stat_updater_queue = Queue.Queue()

    #生产者
    stat_producer_mgr = StatProducerManager(lock, click_url_dict, c_queue=wx_queue, p_queue=article_queue,
                                            c_thread_num=producer_thread_num, p_thread_num=consumer_thread_num)
    #消费者
    stat_consumer_mgr = StatConsumerManager(lock, click_url_dict, c_queue=article_queue, p_queue=stat_updater_queue,
                                            c_thread_num=consumer_thread_num, p_thread_num=stat_updater_thread_num)
    #数据库更新
    stat_updater_mgr = StatUpdaterManager(c_queue=stat_updater_queue, c_thread_num=stat_updater_thread_num)

    stat_producer_mgr.join()
    stat_consumer_mgr.join()
    stat_updater_mgr.join()
    logger.debug('article_stat_spider Finished !')
