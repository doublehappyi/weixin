# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################


from selenium.webdriver.common.keys import Keys
import time
import urlparse

def send_article_url(article_url):
    pass
def get_uin_and_key(lock, driver, click_url):
    # a = 'http://mp.weixin.qq.com/mp/getappmsgext?__biz=MzA5ODM5MDU3MA==&mid=212527034&idx=1&sn=f2d8a560f9dbbd10aa7ba1be25746b74&scene=4'
    # path_url = 'http:// /mp/getappmsgext?__biz=MzA5ODM5MDU3MA==&mid=212527034&idx=1&sn=f2d8a560f9dbbd10aa7ba1be25746b74&scene=4&ct=1442325597&devicetype=android-21&version=&f=json&r=0.09594110562466085&is_need_ad=1&comment_id=4148460220&is_need_reward=0&both_ad=1&reward_uin_count=0&uin=MTcwMDEyNDEzMA%253D%253D&key=c468684b929d2be28066e6e365e3f4f7a9eedfb5831cf71b60b3015134bebf9c721ff5e371954ea8d9ad8f38273fdd43&pass_ticket=AD3cWKvijTRqqJuINxPahGXbhuXauCsGCC28O9h5KwLwCXLfrIptqdDMNl4bJyjL&x5=1'
    # req_url1 ='http://mp.weixin.qq.com/s?__biz=Mjc1NjM3MjY2MA==&mid=226521412&idx=2&sn=b476c9eb48d3248b71380ff59c7aab75&scene=1&srcid=0916uj747S4GWF7tmf7h6GqS#rd&skey=@crypt_9fd05067_f8664965e7830d3b1a8fd65aa1ed477a&deviceid=e305257874773815&pass_ticket=undefined&opcode=2&scene=1&username=@79bc80fd9d6d46562ec097bbbc06b9ee24d93c622cb1c36dc989e8a71f091c0e'
    # req_url_getmsgext ='http://mp.weixin.qq.com/mp/getappmsgext?__biz=Mjc1NjM3MjY2MA==&mid=226521412&idx=2&sn=b476c9eb48d3248b71380ff59c7aab75&scene=1&srcid=0916uj747S4GWF7tmf7h6GqS#rd&skey=@crypt_9fd05067_f8664965e7830d3b1a8fd65aa1ed477a&deviceid=e305257874773815&pass_ticket=undefined&opcode=2&scene=1&username=@79bc80fd9d6d46562ec097bbbc06b9ee24d93c622cb1c36dc989e8a71f091c0e'
    #
    #
    # req_url = 'http://mp.weixin.qq.com/s?__biz=Mjc1NjM3MjY2MA==&mid=226521412&idx=2&sn=b476c9eb48d3248b71380ff59c7aab75&scene=1&srcid=0916uj747S4GWF7tmf7h6GqS#rd&skey=@crypt_9fd05067_f8664965e7830d3b1a8fd65aa1ed477a&deviceid=e305257874773815&pass_ticket=undefined&opcode=2&scene=1&username=@79bc80fd9d6d46562ec097bbbc06b9ee24d93c622cb1c36dc989e8a71f091c0e'
    # https_url = 'https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxcheckurl?requrl=http%3A%2F%2Fmp.weixin.qq.com%2Fs%3F__biz%3DMjc1NjM3MjY2MA%3D%3D%26mid%3D226521412%26idx%3D2%26sn%3Db476c9eb48d3248b71380ff59c7aab75%26scene%3D1%26srcid%3D0916uj747S4GWF7tmf7h6GqS%23rd&skey=%40crypt_9fd05067_f8664965e7830d3b1a8fd65aa1ed477a&deviceid=e305257874773815&pass_ticket=undefined&opcode=2&scene=1&username=@79bc80fd9d6d46562ec097bbbc06b9ee24d93c622cb1c36dc989e8a71f091c0e'
    # #遍历查看是否已经有了该article_url的链接，如果有，直接点击之后解析获取，如果没有则操作浏览器发送，再获取
    #这里可以维护一个字典在内存里面
    # driver.click()
    uin, key = None, None
    with lock:
        driver.switch_to.window(driver.window_handles[-1])
        logger.debug('stat_utils: get_stat_key click_url start: %s' % click_url)
        driver.get(click_url)
        time.sleep(1)
        current_url = driver.current_url
        try:
            key = urlparse.parse_qs(urlparse.urlparse(current_url).query)['key'][0]
            uin = urlparse.parse_qs(urlparse.urlparse(current_url).query)['uin'][0]
        except KeyError as e:
            logger.warning('stat_utils: get_stat_key: no key in current_url: %s, %s' % (current_url, e))
        except Exception as e:
            logger.warning('stat_utils: get_stat_key: Exception: %s' % e)

    return (uin, key)