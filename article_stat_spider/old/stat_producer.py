# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################
import threading
import MySQLdb
import time
import stat_utils
from selenium import webdriver
from selenium.webdriver.common.action_chains import Keys
import random

class StatProducerManager(object):
    def __init__(self, lock, click_url_dict, c_queue, p_queue, c_thread_num, p_thread_num):
        self.lock = lock
        self.click_url_dict = click_url_dict
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(StatProducerWorker(self.lock, self.click_url_dict, self.c_queue, self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('StatProducerWorker:Putting (None) into p_queue')
            self.p_queue.put((None,))

class StatProducerWorker(threading.Thread):
    def __init__(self, lock, click_url_dict, c_queue, p_queue):
        super(StatProducerWorker, self).__init__()
        self.lock = lock
        self.click_url_dict = click_url_dict
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.driver = webdriver.Chrome()
        self.setDaemon(True)
        self.start()

    def run(self):
        #加载微信登录页：这里不需要加锁，初始化的时候，其他线程不会对self.driver进行操作
        login_page = 'https://wx.qq.com/'
        self.driver.get(login_page)
        #确认用户做好了准备工作（打开微信对话窗口），并在终端输入确认：yes
        while 1:
            # confirm = raw_input("If you have done your prepare work, Please enter 'yes':")
            logger.debug('sleep 5 seconds')
            time.sleep(5)
            try:
                logger.debug('Getting title_name ...')
                title_name = self.driver.find_element_by_css_selector('a.title_name').text
                if title_name:
                    break
                else:
                    logger.debug('Please try again')
                    continue
            except Exception as e:
                logger.warning('Please try again, Exception: %s' % e)
        self.driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 't')
        conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db,charset="utf8")
        cursor = conn.cursor()

        while 1:
            wx_row = self.c_queue.get()
            wx_id = wx_row[0]
            if not wx_id:
                logger.debug('StatProducerWorker: no more data in c_queue')
                break
            #这里按顺序来获取
            logger.debug('StatProducerWorker: iter wx_rows: (%s, %s)' % wx_row)
            wx_id, wx_biz = wx_row
            article_select_str = """select article_id, article_url, ref_wx_id from %s """ % settings.tb_article
            article_filter_str = """where ref_wx_id=%s"""
            sql_str = article_select_str + article_filter_str
            cursor.execute(sql_str, wx_id)

            article_rows = cursor.fetchall()
            if not any(article_rows):
                logger.warning('StatProducerWorker: no article in %s' % settings.tb_article)
                continue
            #取最后一个article_row中的article_url
            article_row = article_rows[0]
            article_url = article_row[1]
            logger.debug('StatProducerWorker: send_url:%s' % article_url)

            with self.lock:
                self.driver.switch_to.window(self.driver.window_handles[0])
                self.send_url(article_url)

            time.sleep(1)

            with self.lock:
                if wx_id not in self.click_url_dict:
                    logger.debug('StatProducerWorker put wx_id into click_url_dict start: %s' % wx_id)
                    self.click_url_dict[wx_id] = self.get_click_url(wx_biz)
                    logger.debug('StatProducerWorker put wx_id into click_url_dict end: %s' % self.click_url_dict)

            for article_row in article_rows:
                while self.p_queue.full():
                    logger.debug('StatProducerWorker: p_queue is full, sleep 5 seconds')
                    time.sleep(5)
                logger.debug('StatProducerWorker: put article_row in to p_queue start %s' % str(article_row))
                self.p_queue.put((self.driver, article_row[0], article_row[1].replace('&amp;','&'), article_row[2]))

            logger.debug('StatProducerWorker: click_url_dict: %s' % self.click_url_dict)
            #避免微信限制，这里发送频率不能过快
            logger.debug('StatProducerWorker: sleep 5 seconds')
            time.sleep(5)


    def get_click_url(self, biz):
        biz = biz.replace('==', '%3D%3D')
        click_url = ''
        # base_url = 'https://wx2.qq.com'
        retry = 0
        while 1:
            js_message_plain_elements = self.driver.find_elements_by_css_selector('pre.js_message_plain')
            for el in js_message_plain_elements:
                a = el.find_element_by_tag_name('a')
                href = a.get_attribute('href')
                logger.debug('StatProducerWorker: get_click_url: (biz, href): (%s, %s)' % (biz, href))
                if biz in href:
                    click_url = href
                    break
            if click_url:
                break
            else:
                time.sleep(2)
            if retry > 10:
                logger.debug('StatProducerWorker: get_click_url: failed')
                break
            retry += 1
            logger.debug('StatProducerWorker: get_click_url: retry:%s' % retry)
        return click_url

    def send_url(self, url):
        editArea = self.driver.find_element_by_id('editArea')
        btnSend = self.driver.find_element_by_class_name('btn_send')
        editArea.send_keys(url)
        btnSend.click()
        
        


