# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
# ####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
# ####################日志设置#####################

import Queue
from stat_updater import StatUpdaterManager
from article_producer import ArticleProducerManager
from stat_spider import StatSpiderManager

if __name__ == '__main__':
    stat_spider_thread_num = len(settings.config)
    stat_updater_thread_num = 1

    article_producer_queue = Queue.Queue(stat_spider_thread_num*5)
    stat_updater_queue = Queue.Queue()

    #文章链接生产者
    article_producer_mgr = ArticleProducerManager(p_queue=article_producer_queue, c_thread_num=1, p_thread_num=stat_spider_thread_num)
    #文章爬取消费者
    stat_spider_mgr = StatSpiderManager(c_queue=article_producer_queue, p_queue=stat_updater_queue, c_thread_num=stat_spider_thread_num, p_thread_num=stat_updater_thread_num)
    #数据库更新
    stat_updater_mgr = StatUpdaterManager(c_queue=stat_updater_queue, c_thread_num=stat_updater_thread_num)

    article_producer_mgr.join()
    stat_spider_mgr.join()
    stat_updater_mgr.join()
    logger.debug('main.py Finished !')
