# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################
import threading
import time


class ArticleProducerManager(object):
    def __init__(self, p_queue, c_thread_num, p_thread_num):
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(ArticleProducerWorker(self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()

        for i in xrange(self.p_thread_num):
            logger.debug('ArticleProducerManager:Putting (None, ) into p_queue')
            self.p_queue.put((None,))

class ArticleProducerWorker(threading.Thread):
    def __init__(self, p_queue):
        super(ArticleProducerWorker, self).__init__()
        self.p_queue = p_queue

        self.setDaemon(True)
        self.start()
    def run(self):
        try:
            conn = settings.getConnection()
            cursor = conn.cursor()
            curr_time = int(time.time())
            start_time = curr_time - 5*24*60*60*100
            end_time = curr_time - 4*24*60*60
            article_select_str = "select article_id, article_url, ref_wx_id from {0} where article_publish_time>{1} and article_publish_time <={2} and article_read_num is null".format(settings.db['table']['article'], start_time, end_time)
            cursor.execute(article_select_str)
            for row in cursor:
                while self.p_queue.full():
                    time.sleep(5)
                article_id, article_url, ref_wx_id = row[0], row[1].replace('&amp;', '&'), row[2]
                logger.debug('ArticleProducerWorker: put into p_queue ({0}, {1}, {2})'.format(article_id, article_url, ref_wx_id))
                self.p_queue.put((article_id, article_url, ref_wx_id))

            logger.debug('ArticleProducerWorker: close conn and cursor start')
            conn.close()
            cursor.close()
            logger.debug('ArticleProducerWorker: close conn and cursor end')
        except Exception as e:
            logger.error('ArticleProducerWorker: Exception: %s' % e)




