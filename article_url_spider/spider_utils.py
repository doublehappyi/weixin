# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
import requests_proxy
# ####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
# ####################日志设置#####################

import time
import requests
import json
import urllib
import datetime


def get_article_html_url(wx_biz, config):
    url = None
    try:
        getmasssendmsg ='http://mp.weixin.qq.com/mp/getmasssendmsg?'
        requrl = getmasssendmsg + '__biz=' + wx_biz + '#wechat_webview_type=1&wechat_redirect'
        url = config['base_url'] + '/cgi-bin/mmwebwx-bin/webwxcheckurl?requrl=' + urllib.quote(requrl)+'&' + config['key']
    except Exception as e:
        url = None
        logger.debug('spider_utils: :get_article_html_url Exception {0}'.format(e))
    return url

def get_html_and_url(article_html_url, config):
    html, url = None, None
    try:
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Upgrade-Insecure-Requests': 1,
            'Cookie': config['cookie'],
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36'
        }
        r = requests_proxy.get(article_html_url, headers=headers)
        html, url = r.text, r.url
    except Exception as e:
        html, url = None, None
        logger.debug('spider_utils: :get_html_and_url Exception: article_html_url:{0},E:{1}'.format(article_html_url, e))

    return (html, url)

def get_article_history_url(key, pass_ticket, biz, frommsgid, count=500):
    biz = urllib.quote(biz)
    url = 'http://mp.weixin.qq.com/mp/getmasssendmsg?' + '__biz=' + biz + '&uin=' + biz
    url = url + '&f=json&frommsgid=' + str(frommsgid) + '&count=' + str(count) + '&'
    url = url + 'key=' + key + '&pass_ticket=' + pass_ticket + '&x5=0'
    return url


def getHtmlMsgList(html):
    msgList = []
    r_msgList = 'msgList ='
    msg_list_str = ''

    # 从html中解析出msglist数据
    lines = html.split('\n')
    for line in lines:
        if r_msgList in line:
            msg_list_str = line.strip()
            break
    if not msg_list_str:
        logger.warning('spider_utils: getHtmlMsgList msg_list_str error:html:{0}'.format(html))
        return []

    # 把msglist字符串序列化成字典
    msg_list_str = msg_list_str.replace(r_msgList, '').strip().\
        replace('&quot;', '"').replace('&amp;amp;', '&').replace(';', '').replace('\\\\/', '/').replace("'", '')
    # logger.debug('spider_utils: getHtmlMsgList content_url_count:{0}'.format(msg_list_str.count('content_url')))
    try:
        msg_list_json = json.loads(msg_list_str)
        msgList = msg_list_json['list']
    except ValueError as e:
        msgList = []
        logger.warning('spider_utils: getHtmlMsgList ValueError (%s, %s)' % (e, html))
    except KeyError as e:
        msgList = []
        logger.warning('spider_utils: getHtmlMsgList KeyError (%s, %s)' % (e, html))
    except Exception as e:
        msgList = []
        logger.warning('spider_utils: getHtmlMsgList failed %s' % e)

    return msgList


def put_msgList_into_queue(msgList, queue, wx_id):
    table = settings.db['table']['article']
    for msgItem in msgList:
        try:
            #获取发表日期
            article_publish_time = msgItem.get('comm_msg_info', {}).get('datetime', None)
            #判断是否有key:app_msg_ext_info
            if 'app_msg_ext_info' not in msgItem:
                logger.debug('spider_utils: put_msgList_into_queue: app_msg_ext_info is not exist')
                continue
            #处理外层带图片的文章
            app_msg_ext_info = msgItem['app_msg_ext_info']
            try:
                article_title = app_msg_ext_info['title'].replace('&nbsp', '')
                article_url = app_msg_ext_info['content_url']
                logger.debug('spider_utils: put_msgList_into_queue: put into queue from app_msg_ext_info start: ({0}, {1}, {2}, {3})'.format(table, article_title, article_publish_time, article_url))
                while queue.full():
                    logger.debug('spider_utils: p_queue is full from app_msg_ext_info: sleep 10 seconds')
                    time.sleep(10)
                queue.put((table, article_title, article_url, article_publish_time, wx_id))
                logger.debug('spider_utils: put_msgList_into_queue: put into queue from app_msg_ext_info end :({0}, {1}, {2}, {3})'.format(table, article_title, article_publish_time, article_url))
            except Exception as e:
                logger.warning('spider_utils: put_msgList_into_queue: app_msg_ext_info:Exception:{0}'.format(e))

            #判断是否有key:multi_app_msg_item_list
            if 'multi_app_msg_item_list' not in app_msg_ext_info:
                logger.debug('spider_utils: put_msgList_into_queue: multi_app_msg_item_list is not exist')
                continue
            #获取嵌套内层文章list
            multi_app_msg_item_list = app_msg_ext_info['multi_app_msg_item_list']
            #处理内层文章列表
            for app_msg_item in multi_app_msg_item_list:
                try:
                    article_title = app_msg_item['title'].replace('&nbsp;', '')
                    article_url = app_msg_item['content_url']
                    logger.debug('spider_utils: put_msgList_into_queue: put into queue from app_msg_item start: ({0}, {1}, {2}, {3})'.format(table, article_title, article_publish_time, article_url))
                    while queue.full():
                        logger.debug('spider_utils: p_queue is full from app_msg_item: sleep 10 seconds')
                        time.sleep(10)
                    queue.put((table, article_title, article_url, article_publish_time, wx_id))
                    logger.debug('spider_utils: put_msgList_into_queue: put into queue from app_msg_item end: ({0}, {1}, {2}, {3})'.format(table, article_title, article_publish_time, article_url))
                except KeyError as e:
                    logger.warning('spider_utils: put_msgList_into_queue: app_msg_item:KeyError:{0}'.format(e))
                except Exception as e:
                    logger.warning('spider_utils: put_msgList_into_queue: app_msg_item:Exception:{0}'.format(e))
        except Exception as e:
            logger.warning('spider_utils: put_msgList_into_queue: msgList:Exception:'.format(e))

def get_today_zero_time():
    now = datetime.datetime.now()
    d = datetime.datetime(now.year, now.month, now.day, 0, 0, 0)
    return int(time.mktime(d.timetuple()))

def get_msgList(msgList, start_datetime):
    for i, msg in enumerate(msgList):
        comm_msg_info = msg.get('comm_msg_info', None)
        if not comm_msg_info:
            logger.debug('spider_utils: get_msgList: no comm_msg_info key:{0}'.format(msgList))
            continue

        this_datetime = comm_msg_info.get('datetime', None)
        if not this_datetime:
            continue
        #删掉时间不匹配的数据
        if this_datetime < start_datetime:
            del msgList[i]
            continue

    return msgList

def get_history_msgList(url):
    msgList = []
    try:
        text = requests_proxy.get(url).text
        logger.debug('spider_utils: get_history_msgList: data: {0}'.format(text))
        data = json.loads(text)
        msgList = data['general_msg_list']['list']
    except IndexError as e:
        msgList = []
        logger.error('spider_utils: get_history_msgList: IndexError url:{0},text:{1}E:{2}'.format(url, text, e))
    except Exception as e:
        msgList = []
        logger.error('spider_utils: get_history_msgList: Exception url:{0},E:{1}'.format(url, e))
    return msgList


def isFriend(html):
    r_isFriend = 'isFriend'
    isFriendLine = ''
    lines = html.split('\n')
    for line in lines:
        if r_isFriend in line:
            isFriendLine = line.strip()
            break
    if not isFriendLine:
        logger.debug("spider_utils: isFriend: resolve_isFriend: isFriend is not found :{0}".format(html))
        return False

    try:
        isFriend = isFriendLine.replace(',', '').replace('\'', '').split('=')[1].strip()
        logger.debug("spider_utils: isFriend:{0}".format(isFriend))
        if isFriend == '1':
            isFriend = True
        else:
            isFriend = False
    except IndexError as e:
        logger.debug('spider_utils: isFriend: resolve_isFriend: isFriend ,IndexError:({0}, {1})'.format(isFriend, e))
        isFriend = False

    return isFriend
