# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import Queue
from wx_account_getter import WxAccountGetterManager
from article_history_spider import ArticleHistorySpiderManager
from db_updater import DBUpdaterManager

if __name__ == '__main__':
    wx_account_thread_num = 1
    article_history_spider_thread_num = len(settings.config)
    db_updater_thread_num = 1

    wx_account_queue = Queue.Queue(article_history_spider_thread_num * 50)
    db_updater_queue = Queue.Queue(500)

    wx_account_mgr = WxAccountGetterManager(p_queue=wx_account_queue, c_thread_num=wx_account_thread_num, p_thread_num=article_history_spider_thread_num)
    article_history_spider_mgr = ArticleHistorySpiderManager(c_queue=wx_account_queue, p_queue=db_updater_queue, c_thread_num=article_history_spider_thread_num, p_thread_num=db_updater_thread_num)
    db_updater_mgr = DBUpdaterManager(c_queue=db_updater_queue)

    wx_account_mgr.join()
    article_history_spider_mgr.join()
    db_updater_mgr.join()
    logger.debug('Finished!')
