# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################
import threading
import time


class WxAccountGetterManager(object):
    def __init__(self, p_queue, c_thread_num, p_thread_num):
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(WxAccountGetterWorker(self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()

        for i in xrange(self.p_thread_num):
            logger.debug('WxAccountGetterManager:Putting (None, ) into p_queue')
            self.p_queue.put((None,))


class WxAccountGetterWorker(threading.Thread):
    def __init__(self, p_queue):
        super(WxAccountGetterWorker, self).__init__()
        self.p_queue = p_queue
        self.setDaemon(True)
        self.start()

    def run(self):
        conn = settings.getConnection()
        cursor = conn.cursor()
        try:
            logger.debug('WxAccountGetterWorker: get data from db start')
            select_str = "select wx_id, wx_biz, history_status from {0} where wx_biz is not null and wx_biz != '0'"\
                .format(settings.db['table']['weixin'])
            cursor.execute(select_str)
            logger.debug('WxAccountGetterWorker: get data from db ok')
            for row in cursor:
                while self.p_queue.full():
                    time.sleep(5)
                # wx_id, wx_biz, history_status = row[0], row[1], row[2]
                logger.debug('WxAccountGetterWorker: put into p_queue ({0})'.format(str(row)))
                self.p_queue.put(row)

        except Exception as e:
            logger.error('WxAccountGetterWorker: Exception: %s' % e)

        logger.debug('WxAccountGetterWorker: close conn and cursor start')
        conn.close()
        cursor.close()
        logger.debug('WxAccountGetterWorker: close conn and cursor end')
