html = """<!DOCTYPE html>
<html>
    <head>

	<meta charset="utf-8">
	<script type="text/javascript">
        document.domain = "qq.com";
        window._tegSpeed = [];
        var _wxao = window._wxao || {};
        _wxao.begin = (+new Date());
    </script>
	<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
  	<title>查看历史消息</title>
	<link rel="stylesheet" type="text/css" href="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/style/msg_historical2318b8.css" media="all">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<meta name="referrer" content="origin-when-cross-origin">
<link rel="dns-prefetch" href="//res.wx.qq.com">
<link rel="dns-prefetch" href="//mmbiz.qpic.cn">
<link rel="shortcut icon" type="image/x-icon" href="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/common/favicon22c41b.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<script type="text/javascript">
    String.prototype.html = function(encode) {
        var replace =["&#39;", "'", "&quot;", '"', "&nbsp;", " ", "&gt;", ">", "&lt;", "<", "&amp;", "&", "&yen;", "¥"];
        if (encode) {
            replace.reverse();
        }
        for (var i=0,str=this;i< replace.length;i+= 2) {
             str=str.replace(new RegExp(replace[i],'g'),replace[i+1]);
        }
        return str;
    };

    window.isInWeixinApp = function() {
        return /MicroMessenger/.test(navigator.userAgent);
    };

    window.getQueryFromURL = function(url) {
        url = url || 'http://qq.com/s?a=b#rd'; // 做一层保护，保证URL是合法的
        var query = url.split('?')[1].split('#')[0].split('&'),
            params = {};
        for (var i=0; i<query.length; i++) {
            var arg = query[i].split('=');
            params[arg[0]] = arg[1];
        }
        if (params['pass_ticket']) {
        	params['pass_ticket'] = encodeURIComponent(params['pass_ticket'].html(false).html(false).replace(/\s/g,"+"));
        }
        return params;
    };

    (function() {
	    var params = getQueryFromURL(location.href);
        window.uin = params['uin'] || '';
        window.key = params['key'] || '';
        window.pass_ticket = params['pass_ticket'] || '';
    })();

    window.logs = {
    	pagetime: {}
    };
</script>

        <title></title>

    </head>
    <body id="" class="zh_CN " ontouchstart="">

<div class="msg_page" id="msg_page">

</div>
<div class="loading_wrapper loading" style="display:'';">
	<span class="loadingIcon"></span>加载更多</div>
<div class="more_wrapper no_more" style="display:none">
	<a href="javascript:;" class="more_link">已无更多历史消息</a>
</div>


        <script>window.moon_map = {"new_video/ctl.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/new_video/ctl2853f8.js","biz_wap/utils/device.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/utils/device27f46f.js","biz_wap/jsapi/core.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/jsapi/core275627.js","pages/version4video.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/pages/version4video2853f8.js","biz_common/utils/url/parse.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/url/parse275627.js","biz_common/template-2.0.1-cmd.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/template-2.0.1-cmd275627.js","pages/report.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/pages/report2849a1.js","biz_wap/utils/ajax.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/utils/ajax275627.js","biz_common/dom/class.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/dom/class275627.js","biz_common/dom/event.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/dom/event275627.js","history/webp.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/history/webp2843e5.js","history/localstorage.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/history/localstorage260530.js","biz_common/utils/string/html.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/string/html275627.js","biz_common/utils/string/emoji.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/string/emoji275627.js","history/template_helper.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/history/template_helper24f185.js","history/report.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/history/report24f185.js","history/addEvent.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/history/addEvent2843e5.js","appmsg/outer_link.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/outer_link275627.js","history/lazy.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/history/lazy260a54.js","history/location.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/history/location24f185.js","history/render.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/history/render25624b.js","history/index.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/history/index284861.js"};window.moon_crossorigin = true;</script><script type="text/javascript" src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/moon275627.js" crossorigin></script>

<script type="text/html" id="list">
	{{each list as value keys}}
{{if value && value.comm_msg_info}}
	{{if value.comm_msg_info.type==1}}
	<div class="msg_list">
		<div class="msg_list_hd">
			<h3 class="msg_list_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</h3>
		</div>
		<div class="msg_list_bd">
			<div class="msg_wrapper" msgid="{{value.comm_msg_info.id}}">
				<p class="msg_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</p>
				<div class="text_box msg_inner_wrapper primary_box">
					<div class="msg_item text">
						<p class="msg_text" data-flag="false">{{=value.comm_msg_info.content}}</p>
					</div>
					<span class="box_arrow"></span>
				</div>
			</div>
		</div>
	</div>
	{{else if value.comm_msg_info.type==3}}
	<div class="msg_list">
		<div class="msg_list_hd">
			<h3 class="msg_list_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</h3>
		</div>
		<div class="msg_list_bd">
			<div id="WXVOICE{{value.comm_msg_info.id}}" class="msg_wrapper" msgid="{{value.comm_msg_info.id}}">
					<p class="msg_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</p>
					<a href="javascript:;" class="msg_inner_wrapper primary_box">
					<div class="msg_item img" data-msgid="{{value.comm_msg_info.id}}" data-type="imgp">
						<span class="msg_img">
							<img data-src="/mp/getmediadata?__biz={{biz}}&type=img&mode=small&msgid={{value.comm_msg_info.id}}&uin={{uin}}&key={{key}}" alt="" data-msgid="{{value.comm_msg_info.id}}" data-s="640"  data-t="{{value.comm_msg_info.datetime*1000}}">
						</span>
					</div>
				</a>
			</div>
		</div>
	</div>
	{{else if value.comm_msg_info.type==34}}
	<div class="msg_list">
		<div class="msg_list_hd">
			<h3 class="msg_list_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</h3>
		</div>
		<div class="msg_list_bd">
			<div id="WXVOICE{{value.comm_msg_info.id}}" class="msg_wrapper" msgid="{{value.comm_msg_info.id}}">
					<p class="msg_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</p>
					<a href="javascript:void(0);"  class="voice_box msg_inner_wrapper primary_box">
					<div class="msg_item voice" length="{{value.voice_msg_ext_info.play_length}}" data-flag="false">
						<span class="voice_point">
							<i class="add_on_icon voice"></i>
							<audio fileid="{{value.voice_msg_ext_info.fileid}}" preload type="audio/mpeg" src="/mp/getmediadata?__biz={{biz}}&type=voice&msgid={{value.comm_msg_info.id}}&uin={{uin}}&key={{key}}"  alt="">not support</audio>
						</span>
						<span class="msg_desc"></span>
					</div>
					<span class="box_arrow"></span>
				</a>
			</div>
		</div>
	</div>
	{{else if value.comm_msg_info.type==49}}
	<div class="msg_list">
		<div class="msg_list_hd">
			<h3 class="msg_list_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</h3>
		</div>
		<div class="msg_list_bd">
			{{if value.app_msg_ext_info.is_multi==0}}
			<div id="WXAPPMSG{{value.comm_msg_info.id}}" class="msg_wrapper" msgid="{{value.comm_msg_info.id}}">
				<p class="msg_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</p>
  				<div class="msg_inner_wrapper default_box news_box">
	    			<a class="msg_item news redirect" hrefs="{{value.app_msg_ext_info.content_url}}">
	     				<div class="msg_item_hd">
							<h4 class="msg_title">{{value.app_msg_ext_info.title}}</h4>
						</div>
						<div class="msg_item_bd">
							<div class="msg_cover" hrefs="{{value.app_msg_ext_info.content_url}}">
								<img data-src="{{value.app_msg_ext_info.cover}}" alt="" data-s="640" data-t="{{value.comm_msg_info.datetime*1000}}" hrefs="{{value.app_msg_ext_info.content_url}}">
							</div>
							<p class="msg_desc" hrefs="{{value.app_msg_ext_info.content_url}}">
								{{=value.app_msg_ext_info.digest}}
							</p>
						</div>
						<div class="msg_item_ft" hrefs="{{value.app_msg_ext_info.content_url}}">
							阅读原文<span class="add_on_icon arrow"></span>
						</div>
					</a>
				</div>
			</div>
			{{else}}
			<div id="WXAPPMSG{{value.comm_msg_info.id}}" class="msg_wrapper" msgid="{{value.comm_msg_info.id}}">
					<p class="msg_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</p>
					<div class="msg_inner_wrapper default_box news_box">
					<div class="msg_item multi_news">
						<a class="msg_cover redirect" hrefs="{{value.app_msg_ext_info.content_url}}">
							<img data-src="{{value.app_msg_ext_info.cover}}" alt="" data-s="640" data-t="{{value.comm_msg_info.datetime*1000}}" hrefs="{{value.app_msg_ext_info.content_url}}">
							<div class="msg_title_bar">
								<h4 class="msg_title" hrefs="{{value.app_msg_ext_info.content_url}}">{{value.app_msg_ext_info.title}}</h4>
							</div>
						</a>
						{{each value.app_msg_ext_info.multi_app_msg_item_list as subvalue subkey}}
							<div class="sub_msg_list">
								<a class="sub_msg_item redirect" hrefs="{{subvalue.content_url}}">
									<span class="thumb">
										<img data-src="{{subvalue.cover}}" alt="" style="width:50px;height:50px;" data-s="300" data-t="{{value.comm_msg_info.datetime*1000}}">
									</span>
									<h4 class="msg_title" hrefs="{{subvalue.content_url}}">{{subvalue.title}}</h4>
								</a>
							</div>
						{{/each}}
					</div>
				</div>
			</div>
			{{/if}}
		</div>
	</div>
	{{else if value.comm_msg_info.type==62}}
	<div class="msg_list">
		<div class="msg_list_hd">
			<h3 class="msg_list_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</h3>
		</div>
		<div class="msg_list_bd">
			<div id="WXVIDEO{{value.comm_msg_info.id}}" class="msg_wrapper" msgid="{{value.comm_msg_info.id}}">
				<p class="msg_date">{{dateFormat value.comm_msg_info.datetime*1000 'yyyy年M月d日hh:mm'}}</p>
				<iframe class="video_iframe" style="" height="" width="" frameborder="0" data-src="https://mp.weixin.qq.com/mp/getcdnvideourl?__biz={{biz}}&cdn_videoid={{value.video_msg_ext_info.cdn_videoid}}&thumb={{value.video_msg_ext_info.thumb}}&uin={{uin}}&key={{key}}" allowfullscreen=""></iframe>
			</div>
		</div>
	</div>
	{{/if}}
{{/if}}
	{{/each}}
</script>

<script type="text/javascript">
	window.onerror = function(msg, url, line, col, error){
		if(location.href.indexOf('sharedebug')>-1){
			alert(msg);
		}
		//没有URL不上报！上报也不知道错误
	    if (msg != "Script error." && !url){
	        return true;
	    }
		function getUA(){
            return navigator.userAgent;
        }
        function getOS(ua){
            var oss = [/android/gi, /iPhone/gi,/Windows/gi];
            var i, o;
            var ua = ua || getUA();
            for (i = 0; i < oss.length; i++) {
                o = ua.match(oss[i]);
                if (o) {
                    return o[0];
                }
            }
            return 'unknown';
        }
        function getBrowser(ua){
            //ie5、ie6、ie7、ie8、ie9、ie10、qq、chrome、firefox、safari、se360、theworld、maxthon、other
            var browsers = [/mqqbrowser/gi,/Chrome/gi,/safari/gi,/MSIE/gi];
            var b, i;
            var ua = ua || getUA();
            for (i = 0; i < browsers.length; i++) {
                b = ua.match(browsers[i]);
                if (b) {
                    return b[0];
                }
            }
            return 'unknown';
        }
        function getMetered(){
            //微信浏览器把nettype放入了useragent
            var ua = getUA();
            var netType = [/WIFI/gi,/3G/gi,/2G/gi];
            for (var i = 0; i < netType.length; i++) {
                var b = ua.match(netType[i]);
                if (b) {
                    return b[0];
                }
            };
            return 'other';
        }
        function parseUrl(url) {
            var a =  document.createElement('a');
            a.href = url;
            return {
                source: url,
                protocol: a.protocol.replace(':',''),
                host: a.hostname,
                port: a.port,
                query: a.search,
                params: (function(){
                    var ret = {},
                        seg = a.search.replace(/^\?/,'').split('&'),
                        len = seg.length, i = 0, s;
                    for (;i<len;i++) {
                        if (!seg[i]) { continue; }
                        s = seg[i].split('=');
                        ret[s[0]] = s[1];
                    }
                    return ret;
                })(),
                file: (a.pathname.match(/([^\/?#]+)$/i) || [,''])[1],
                hash: a.hash.replace('#',''),
                path: a.pathname.replace(/^([^\/])/,'/$1'),
                relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
                segments: a.pathname.replace(/^\//,'').split('/')
            };
        }
        function obj2query(obj) {
            var arr = [];
            for(var key in obj) {
                arr.push(key + "=" + encodeURIComponent(obj[key]||""));
            }
            return arr.join("&");
        }
        function getUrl(){
            var href = location.href,
                parseUrls = parseUrl(href),
                params = parseUrls.params;
            //href中包涵了uin、key、devicetype、version、lang这几个登录态多余信息
            delete params.uin;
            delete params.key;
            delete params.devicetype;
            delete params.version;
            delete params.lang;
            delete params.from;
            delete params.isappinstalled;
            delete params.pass_ticket;
            var reurl = parseUrls.protocol + '://' + parseUrls.host + parseUrls.path + '?' + obj2query(params);
            return reurl;
        }
        //不一定所有浏览器都支持col参数
        var col = col || (window.event && window.event.errorCharacter) || 0;
        var data = {
        	from :'wap',
        	path: "/mp/getmasssendmsg", //发生错误的页面
			file: getUrl(), //发生错误的文件(定位到js)
			line: line, //行号
			col: col, //出错位置
			msg: msg, //出错信息
			uin: 'MTcwMDEyNDEzMA==',
			ua: getBrowser(),
			os: getOS(),
			net:getMetered()
        };

        if (!!error && !!error.stack){
            //如果浏览器有堆栈信息
            //直接使用
            data.msg = error.message;
        }else if (!!arguments.callee){
            //尝试通过callee拿堆栈信息
            var ext = [];
            var f = arguments.callee.caller, c = 3;
            //这里只拿三层堆栈信息
            while (f && (--c>0)) {
               ext.push(f.toString());
               if (f  === f.caller) {
                    break;//如果有环
               }
               f = f.caller;
            }
            ext = ext.join(" ");//wowo那边不能有；，：
            data.msg = ext;
        }
        var tmp = 'path:'+data.path+';file:'+data.file+';line:'+data.line+';msg:'+data.msg+';uin:'+data.uin+';col:'+data.col+';from:wap;';

        var reportUrl = 'https://mp.weixin.qq.com/mp/speedreport?data_type=4&net='+data.net+'&os='+data.os+'&ua='+data.ua+'&ext_data='+encodeURIComponent(tmp);
        //console.log(reportUrl);
        setTimeout(function(){
        	var _img = new Image(1, 1);
        	_img.src = reportUrl;
        },16);
	}

	var minMsgId = 0xffffffff,
		loading = document.querySelector('.loading'),
		images = [],
		isLoading = false,
        windowwx_css = "http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/style/page/winwx/history284861.css",
		iswp = !!(navigator.userAgent.match(/Windows\sPhone/i)),
		isContinue = '1',
    	biz = 'MzA5ODM5MDU3MA==',
    	key = 'dffc561732c22651123e9f981dcd74fd2ec8e81570283f7a7f59c94d5663e3abff479878e828aa82bfffb60f039ff7d0',
    	uin = 'MTcwMDEyNDEzMA==',
    	isFriend = '1',
    	clipHeight = 300,
    	    	msgList = '{&quot;list&quot;:[{&quot;comm_msg_info&quot;:{&quot;id&quot;:212670809,&quot;type&quot;:49,&quot;datetime&quot;:1442149418,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;为什么看起来不是很复杂的网站，淘宝、腾讯却需要大量顶尖高手来开发？&quot;,&quot;digest&quot;:&quot;就拿淘宝来说说，当作给新人一些科普。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:207997542,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212670806&amp;amp;idx=1&amp;amp;sn=149d9465415cc50f3b5c1bd32cd54888&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B2Diaibiaxu9SLK5cmb3ObIiawRjjys2gIib2GA1wkRibQ07ANrJd3fo4lWJcKr3JfKvicM2ialvibUaqXzdTw\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;&quot;}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:212661911,&quot;type&quot;:49,&quot;datetime&quot;:1442066098,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;SQL&nbsp;新手指南&quot;,&quot;digest&quot;:&quot;（点击上方蓝字，快速关注我们)英文：Udemy译者：腊八粥网址：http:\\/\\/www.labazhou.ne&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:205613628,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212661908&amp;amp;idx=1&amp;amp;sn=1e266f4f282cd2f9f49c5a8c0ccaf804&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B1T4NA703TJKWUiaHcpzyJaFJ3TINjZSZTna6FwBMuEibdNHnCp7lnZhGrrY559KFURHYicjhr6AESZw\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:1,&quot;multi_app_msg_item_list&quot;:[{&quot;title&quot;:&quot;Debsources：观察自由软件的二十年历史&quot;,&quot;digest&quot;:&quot;（点击上方蓝字，快速关注我们)作者：奇客-WinterIsComing网址：http:\\/\\/www.solid&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:212661758,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212661908&amp;amp;idx=2&amp;amp;sn=9fe2fedf0d16176a364c3f7381f79592&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B1T4NA703TJKWUiaHcpzyJaFXAOuTYibHCl3Jo42Xg0NJLgO64CU9ezCqgKxficTRwKVvCXqsT0dwibYg\\/0?wx_fmt=gif&quot;,&quot;author&quot;:&quot;&quot;}],&quot;author&quot;:&quot;&quot;}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:212650824,&quot;type&quot;:49,&quot;datetime&quot;:1441978011,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;RAID&nbsp;技术介绍和总结&quot;,&quot;digest&quot;:&quot;RAID是一个我们经常能见到的名词。但却因为很少能在实际环境中体验，所以很难对其原理&nbsp;能有很清楚的认识和掌握。本文将对RAID技术进行介绍和总结，以期能尽量阐明其概念。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:212650300,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212650821&amp;amp;idx=1&amp;amp;sn=8c1a23f6cf709bf4a95a3b1d4b496cfa&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B0RLW85zDxYCcmyCys5uP56awy3WJp2IqBYFp2TaTYEhSF886mL5ibicKjUzOicoIuDRo41ywrJC11icg\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:1,&quot;multi_app_msg_item_list&quot;:[{&quot;title&quot;:&quot;MIT&nbsp;研究人员透露新的缓存一致性机制&quot;,&quot;digest&quot;:&quot;（点击上方蓝字，快速关注我们)作者：奇客-WinterIsComing网址：http:\\/\\/www.solid&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:212650644,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212650821&amp;amp;idx=2&amp;amp;sn=1c7f9555f22d2618609c11cae53511bc&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B0RLW85zDxYCcmyCys5uP56O3YeJrCkAp0lmXIKaHq3oibZFJu51nssYNibu8HXcYRfKOQetBtC0Omw\\/0?wx_fmt=gif&quot;,&quot;author&quot;:&quot;&quot;}],&quot;author&quot;:&quot;&quot;}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:212636310,&quot;type&quot;:49,&quot;datetime&quot;:1441900398,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;史上最全的&nbsp;MSSQL&nbsp;复习笔记&nbsp;（上）&quot;,&quot;digest&quot;:&quot;sql语言：结构化的查询语言。（Structured&nbsp;Query&nbsp;Language），是关系数据库管理系统的标准语言。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:207230770,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212636307&amp;amp;idx=1&amp;amp;sn=8a6f62d78965e2b7b514d851d93d51ab&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B1iaTRiadcdA5OQj6ZvYxAkhaziaf6KicfYvPmjRzmv6aJg2Uo5sJlcKSsnEdJV1SDmlCqVfvAqnCkEew\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:1,&quot;multi_app_msg_item_list&quot;:[{&quot;title&quot;:&quot;史上最全的&nbsp;MSSQL&nbsp;复习笔记&nbsp;（下）&quot;,&quot;digest&quot;:&quot;（点击上方蓝字，快速关注我们)作者：游戏世界网址：http:\\/\\/www.cnblogs.com\\/gamewo&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:207614258,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212636307&amp;amp;idx=2&amp;amp;sn=e81434140f124c87bcd203afd6ca2418&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B1iaTRiadcdA5OQj6ZvYxAkhawOCI9vKPg7mJgBYsPne9XX9EoPf3CxFT8xkRbsGlgHwIyYJuj3XDQw\\/0?wx_fmt=jpeg&quot;,&quot;author&quot;:&quot;&quot;}],&quot;author&quot;:&quot;&quot;}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:212617090,&quot;type&quot;:49,&quot;datetime&quot;:1441802943,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;架构之路（二）：性能&quot;,&quot;digest&quot;:&quot;我们在上一篇博客中设定了架构的目标，只有一个，就是可维护性。完全没有提性能，这是故意的。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:212603193,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212617086&amp;amp;idx=1&amp;amp;sn=801996c38812fa2eb434e4d7a2e665e1&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B1xGKSicwwY9OyEYVtlibgxLnG8ZXqwCAflVYMnt5cK3FWmtWs6Nbb9f4Xg3aQLgd1N1m2ibrNpyNvqw\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:1,&quot;multi_app_msg_item_list&quot;:[{&quot;title&quot;:&quot;迈克菲竞选美国总统&quot;,&quot;digest&quot;:&quot;（点击上方蓝字，快速关注我们)作者：奇客-WinterIsComing网址：http:\\/\\/www.solid&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:212615994,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212617086&amp;amp;idx=2&amp;amp;sn=3e90212c734620b16880d1e23a38a574&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B1xGKSicwwY9OyEYVtlibgxLn1qiaUvtvyFx2NHTNGvooTJdiaPJFzwcLoRr7ibibdSLxjdVmCNMkpFe4eA\\/0?wx_fmt=jpeg&quot;,&quot;author&quot;:&quot;&quot;}],&quot;author&quot;:&quot;&quot;}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:212603597,&quot;type&quot;:49,&quot;datetime&quot;:1441717221,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;架构之路（一）：目标&quot;,&quot;digest&quot;:&quot;（点击上方蓝字，快速关注我们)作者：自由飞网址：http:\\/\\/www.cnblogs.com\\/freefly&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:212603193,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212603594&amp;amp;idx=1&amp;amp;sn=935f08803d656a2c8dc4243a2cb9765d&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B3TwRzKewAvibviavSSTzKlYgEoIRSDnLLx2QaiaQkE15OrKzicTBB7T5rAnQsIUoP0icTI3pbes9UicicKw\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:1,&quot;multi_app_msg_item_list&quot;:[{&quot;title&quot;:&quot;RSA&nbsp;实现漏洞暴露&nbsp;HTTPS&nbsp;网站的私钥&quot;,&quot;digest&quot;:&quot;（点击上方蓝字，快速关注我们)作者：奇客-Winteriscoming网址：http:\\/\\/www.solid&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:212603372,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212603594&amp;amp;idx=2&amp;amp;sn=204c0a92daaab0a9a3792f4694306cc6&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B3TwRzKewAvibviavSSTzKlYgAQP7Qws1SwZGA1UfoI83ibJesYibc9A7kNb8RNWn0pV2ia0tFwKAWnYIA\\/0?wx_fmt=gif&quot;,&quot;author&quot;:&quot;&quot;}],&quot;author&quot;:&quot;&quot;}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:212588633,&quot;type&quot;:49,&quot;datetime&quot;:1441635081,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;揭开&nbsp;Socket&nbsp;编程的面纱&quot;,&quot;digest&quot;:&quot;对TCP\\/IP、UDP、Socket编程这些词你不会很陌生吧？随着网络技术的发展，这些词充斥着我们的耳朵。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:211695239,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212588630&amp;amp;idx=1&amp;amp;sn=64bba1481f09a0bcae68d0da889909d3&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B2tNzJpVOuzt5bU6HaY30KxVt8kbbEUFW8B68bbU5LxOXeHU0AiaE8AyWBloxDiad53UMjIib3tP3IGQ\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;&quot;}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:212574826,&quot;type&quot;:49,&quot;datetime&quot;:1441544179,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;Appboy&nbsp;基于&nbsp;MongoDB&nbsp;的数据密集型实践&quot;,&quot;digest&quot;:&quot;本文摘录自Appboy联合创始人兼CIO&nbsp;Jon&nbsp;Hyman在MongoDB&nbsp;World&nbsp;2015上的演讲。Appboy正在过手机等新兴渠道尝试一种新的方法，让机构可以与顾客建立更好的关系，可以说是市场自动化产业的一个前沿探索者。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:205610560,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212574823&amp;amp;idx=1&amp;amp;sn=d4755cb9f0ae0d211b8d423d96adc453&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B3bL4ic2gXo4AkaFdkicGGlKAOJFZwDziaRf5k3BYXjuys1x7TsxN44cld1mNOmE3RaBWjC10X3G7MuQ\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;&quot;}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:212561252,&quot;type&quot;:49,&quot;datetime&quot;:1441459723,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;构建需求响应式亿级商品详情页&quot;,&quot;digest&quot;:&quot;目前商品详情页个性化需求非常多，数据来源也是非常多的，而且许多基础服务做不了的都放我们这，因此我们需要一种架构能快速响应和优雅的解决这些需求问题。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:212560707,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212561249&amp;amp;idx=1&amp;amp;sn=c654bf94e4a9480a6786327df83021ef&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;http:\\/\\/www.lagou.com\\/custom\\/list.html?utm_source=AD__blzx&amp;amp;utm_medium=article&amp;amp;utm_campaign=toufang&amp;amp;m=1&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B2WAHpmOJ9niatFwHARyMRzCiaP50WxC9TLfnZk1gBpODbfo5I0XxeLryMvWyc8WKCI7GEHPt2jQ0gg\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;&quot;}},{&quot;comm_msg_info&quot;:{&quot;id&quot;:212544565,&quot;type&quot;:49,&quot;datetime&quot;:1441370941,&quot;fakeid&quot;:&quot;3098390570&quot;,&quot;status&quot;:2,&quot;content&quot;:&quot;&quot;},&quot;app_msg_ext_info&quot;:{&quot;title&quot;:&quot;可伸缩Web架构与分布式系统（2）&quot;,&quot;digest&quot;:&quot;上文谈及了在设计分布式系统中需要考虑的一些核心问题，现在让我们来聊聊比较困难的部分：访问数据的可伸缩性。&quot;,&quot;content&quot;:&quot;&quot;,&quot;fileid&quot;:212510563,&quot;content_url&quot;:&quot;http:\\/\\/mp.weixin.qq.com\\/s?__biz=MzA5ODM5MDU3MA==&amp;amp;mid=212544562&amp;amp;idx=1&amp;amp;sn=b84098ecad2220af515ed628c0a63bf3&amp;amp;scene=4#wechat_redirect&quot;,&quot;source_url&quot;:&quot;&quot;,&quot;cover&quot;:&quot;http:\\/\\/mmbiz.qpic.cn\\/mmbiz\\/DmibiaFiaAI4B1ODeoqw9rtateAA1qThMlUyeDmadQ7iarsw3cQHFFdPYic5gm72kGiaA3UOQPNZDErgzd3icQib2v8DxA\\/0?wx_fmt=jpeg&quot;,&quot;subtype&quot;:0,&quot;is_multi&quot;:0,&quot;multi_app_msg_item_list&quot;:[],&quot;author&quot;:&quot;&quot;}}]}';
    	    	seajs.use("history/index.js");
</script>

    </body>
</html>

"""