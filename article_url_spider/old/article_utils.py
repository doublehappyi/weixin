# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
# ####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import requests
import time, datetime

#重试请求
def get_response(url):
    ret = None
    retry = 0
    while 1:
        try:
            r = requests.get(url, allow_redirects=False)
            status_code = r.status_code

            logger.debug('article_utils: get_response status_code: %s, type:%s' % (status_code, type(status_code)))
            if status_code == 200 or status_code == 301 or status_code == 302:
                ret = r
            else:
                logger.debug('article_utils: get_response error %s' % url)
                return None
            break
        except requests.exceptions.Timeout as e:
            logger.warning('article_utils: get_response requests.exceptions.Timeout %s' % e)
            retry += 1
            if retry > settings.max_retry:
                break
            logger.warning('article_utils: get_response retry times %s' % retry)
            continue
        except Exception as e:
            logger.debug('article_utils: get_response url, exception (%s, %s)' % (url, e))
    return ret


# def get_article_queue_data(queue, msgList, wx_id, table, datetime):
def get_article_queue_data(msgList, wx_id, table, datetime):
    if not datetime:
        datetime = get_today_zero_time() - 30*24*60*60
    ret = []
    # logger.debug('article_utils: get_article_queue_data: len(msgList) : %s, %s' % (len(msgList), str(msgList)))
    logger.debug('article_utils: get_article_queue_data: len(msgList) : %s' % len(msgList))

    for idx in xrange(len(msgList)):
        try:
            msgItem = msgList[idx]
            #获取发表日期
            article_publish_time = msgItem.get('comm_msg_info', {}).get('datetime', None)
            #如果传入了datetime参数（不为None），并且文章发布时间在datetime之前，则不予放入队列
            if datetime and article_publish_time < datetime:
                logger.debug('article_utils: get_article_queue_data: not today msg')
                continue
            #判断是否有key:app_msg_ext_info
            if 'app_msg_ext_info' not in msgItem:
                logger.debug('article_utils: get_article_queue_data: app_msg_ext_info is not exist')
                continue
            #处理外层带图片的文章
            app_msg_ext_info = msgItem['app_msg_ext_info']
            logger.debug('article_utils: get_article_queue_data: Putting content_url from app_msg_ext_info: %s' % app_msg_ext_info['content_url'])
            try:
                article_title = app_msg_ext_info['title'].replace('&nbsp', '')
                article_url = app_msg_ext_info['content_url']
                logger.debug('article_utils: get_article_queue_data: put into ret from app_msg_ext_info start: (%s, %s, %s, %s)' %
                             (article_title, article_publish_time, article_url, wx_id))
                ret.append((table, article_title, article_url, article_publish_time, wx_id))
                logger.debug('article_utils: get_article_queue_data: put into ret from app_msg_ext_info: end(%s, %s, %s, %s)' %
                             (article_title, article_publish_time, article_url, wx_id))
            except Exception as e:
                logger.warning('article_utils: get_article_queue_data: app_msg_ext_info:Exception:%s' % e)
            #判断是否有key:multi_app_msg_item_list
            if 'multi_app_msg_item_list' not in app_msg_ext_info:
                logger.debug('article_utils: get_article_queue_data: multi_app_msg_item_list is not exist')
                continue

            #获取嵌套内层文章lists
            multi_app_msg_item_list = app_msg_ext_info['multi_app_msg_item_list']
            logger.debug('article_utils: get_article_queue_data: len(multi_app_msg_item_list) : %s' % len(multi_app_msg_item_list))
            #处理内层文章列表
            for app_msg_item in multi_app_msg_item_list:
                try:
                    article_title = app_msg_item['title'].replace('&nbsp', '')
                    article_url = app_msg_item['content_url']
                    logger.debug('article_utils: get_article_queue_data: put into ret from app_msg_item start: (%s, %s, %s, %s)' %
                                 (article_title, article_publish_time, article_url, wx_id))
                    ret.append((table, article_title, article_url, article_publish_time, wx_id))
                    logger.debug('article_utils: get_article_queue_data: put into ret from app_msg_item end: (%s, %s, %s, %s)' %
                                 (article_title, article_publish_time, article_url, wx_id))
                except Exception as e:
                    logger.warning('article_utils: get_article_queue_data: app_msg_item:Exception:%s' % e)
        except Exception as e:
            logger.warning('article_utils: get_article_queue_data: msgList:Exception:%s' % e)
    return ret

def get_today_zero_time():
    now = datetime.datetime.now()
    d = datetime.datetime(now.year, now.month, now.day, 0, 0, 0)
    return int(time.mktime(d.timetuple()))