# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
import MySQLdb
import time


class ArticleUrlUpdater(threading.Thread):
    def __init__(self, c_queue):
        super(ArticleUrlUpdater, self).__init__()
        self.c_queue = c_queue
        self.conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db,charset="utf8")

        self.cursor = self.conn.cursor()
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            try:
                #从队列里面取数据
                logger.debug("ArticleUrlUpdater: Getting data from c_queue start")
                task = self.c_queue.get()
                logger.debug("ArticleUrlUpdater: Getting data from c_queue end %s" % str(task))
                table = task[0]
                logger.debug('ArticleUrlUpdater:table: %s' % table)
                #判断id是否可用
                if not table:
                    logger.debug('ArticleUrlUpdater: no more data in c_queue')
                    break

                if table == settings.tb_article:
                    self.update_article(task)
                elif table == settings.tb_weixin:
                    self.update_weixin(task)
            except Exception,e:
                logger.error("Exception in ArticleUrlUpdater: %s" % e)
            finally:
                self.c_queue.task_done()

        logger.debug('closing db conn, cursor')
        self.cursor.close()
        self.conn.close()
        logger.debug('closing db conn, cursor OK')

    def update_weixin(self, task):
        try:
            #从队列里面取数据
            logger.debug("ArticleUrlUpdater: update_weixin: Getting data from c_queue start")
            table, history_status, wx_id = task
            logger.debug("ArticleUrlUpdater: update_weixin: Getting data from c_queue end (%s, %s, %s)" % task)

            #更新到数据库
            update_time = int(time.time())
            logger.debug('ArticleUrlUpdater: update_weixin: update table start (%s, %s, %s)' % task)
            sql_str = """update %s t set t.history_status=%d, t.update_time=%d where wx_id=%d""" % (table, history_status, update_time, wx_id)
            self.cursor.execute(sql_str)
            self.conn.commit()
            logger.debug('ArticleUrlUpdater: update_weixin: update table end: (%s, %s, %s)' % task)

        except Exception,e:
            logger.error("ArticleUrlUpdater:update_weixin: Exception: %s" % e)

    def update_article(self, task):
        try:
            #从队列里面取数据
            logger.debug("ArticleUrlUpdater: update_article: Getting data from c_queue start")
            table, article_title, article_url, article_publish_time, ref_wx_id = task
            logger.debug("ArticleUrlUpdater: update_article: Getting data from c_queue end (%s, %s, %s, %s, %s)" % task)

            #查询数据库是否已经有对应的文章链接了，如果有了，就不再做插入操作。
            select_str = """select article_id from %s where """ % table
            filter_str = """article_url=%s"""
            select_sql_str = select_str + filter_str
            self.cursor.execute(select_sql_str, article_url)
            rows = self.cursor.fetchall()
            if rows:
                logger.debug('ArticleUrlUpdater: update_article: duplicated entry :(%s, %s)' % (rows[0], article_url))
                return

            #更新到数据库
            update_time = int(time.time())
            logger.debug('ArticleUrlUpdater: update_article: insert into table start (%s, %s, %s, %s, %s)' % task)
            insert_str = """insert into %s values """ % table
            values_str = """(null, %s, %s, null, null, %s, %s, %s)"""
            sql_str = insert_str + values_str
            self.cursor.execute(sql_str, (article_title, article_url, article_publish_time, update_time, ref_wx_id))
            self.conn.commit()
            logger.debug('ArticleUrlUpdater: update_article: insert into table end (%s, %s, %s, %s, %s)' % task)

        except Exception,e:
            logger.error("ArticleUrlUpdater: update_article: Exception: %s" % e)


            



