# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
import urllib2
from selenium import webdriver
import re
import time
import article_utils, article_html, article_history

class ArticleUrlManager(object):
    def __init__(self, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(ArticleUrlWorker(self.c_queue, self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('ArticleUrlManager:Putting (None) into p_queue')
            self.p_queue.put((None,))


class ArticleUrlWorker(threading.Thread):
    def __init__(self, c_queue, p_queue):
        super(ArticleUrlWorker, self).__init__()
        self.c_queue = c_queue
        self.p_queue = p_queue
        # self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome()
        self.setDaemon(True)
        self.start()

    def run(self):
        self.driver.get('https://wx.qq.com/')
        link, ng_hrefs = self.get_link_ng_hrefs()
        if not link or not ng_hrefs:
            logger.debug('ArticleUrlWorker:ng_href error!')
            return
        self.switch_to_new_window(link)
        
        idx_ng_hrefs, len_ng_hrefs = 0, len(ng_hrefs)
        while 1:
            ng_href = ng_hrefs[idx_ng_hrefs % len_ng_hrefs]
            logger.debug('ArticleUrlWorker:get row start')
            row = self.c_queue.get()
            self.c_queue.task_done()
            logger.debug('ArticleUrlWorker:get row end {0}'.format(str(row)))
            if row[0] is None:
                logger.debug('ArticleUrlWorker: no more data in c_queue')
                break
            wx_id, wx_biz, history_status = row[0], row[1], int(row[2])
            #利用wx_biz重组ng_href，生成https请求，利用webdriver取回重定向的历史消息页面history_redirect_url
            history_redirect_url = self.get_history_redirect_url(ng_href, wx_biz)
            #如获取失败，则忽略该请求，循环重新执行
            if not history_redirect_url:
                logger.debug('ArticleUrlWorker:get_history_redirect_url failed wx_biz is :%s' % wx_biz)
                continue
            logger.debug('ArticleUrlWorker:get_history_redirect_url success:%s' % history_redirect_url)
            #获取页面html，如果获取失败，则忽略该wx_id，循环继续
            html_res = article_utils.get_response(history_redirect_url)
            if not html_res:
                logger.warning("ArticleUrlWorker: get_html: failed (%s, %s)" % (wx_biz, history_redirect_url))
                continue
            logger.debug('ArticleUrlWorker: get_html: success (%s, %s)' % (wx_biz, history_redirect_url))
            isFriend = article_html.resolve_isFriend(html_res.text)
            html_msgList = article_html.resolve_msgList(html_res.text, history_redirect_url)
            if not html_msgList:
                logger.warning("ArticleUrlWorker: resolve_html: failed (%s, %s)" % (wx_biz, history_redirect_url))
                continue

            frommsgid = html_msgList[-1].get('comm_msg_info').get('id')
            logger.debug('ArticleUrlWorker:isFriend, history_status， frommsgid : (%s, %s, %s)' % (isFriend, history_status, frommsgid))
            #history_status有3个值：#0代表从未获取过数据,1代表获取过第一页数据,2代表已获取过历史数据
            #优先判断是否已经拉取过历史数据：因为如果history_status==0后续会对history_status做处理，而history_status=0的时候，已经把最新的数据抓取过了。
            if history_status == 2:
                logger.debug('ArticleUrlWorker: history_status == 2')
                today_zero_time = article_utils.get_today_zero_time()
                today_queue_data = article_utils.get_article_queue_data(html_msgList, wx_id, settings.tb_article, today_zero_time)
                logger.debug('ArticleUrlWorker: today_queue_data length (wx_id: %s, len(today_queue_data):%s)' % (wx_id, len(today_queue_data)))
                for data in today_queue_data:
                    logger.debug('ArticleUrlWorker: put today_data into queue start (%s, %s, %s, %s, %s)' % data)
                    self.p_queue.put(data)
                    logger.debug('ArticleUrlWorker: put today_data into queue end (%s, %s, %s, %s, %s)' % data)

            #再判断是否从来没有拉取过数据
            if history_status == 0:
                logger.debug('ArticleUrlWorker: history_status == 0')
                #获取html中的list数据
                logger.debug('ArticleUrlWorker: resolve_html:add_msgList_to_queue html start')
                html_queue_data = article_utils.get_article_queue_data(html_msgList, wx_id, settings.tb_article, None)
                logger.debug('ArticleUrlWorker: html_queue_data length: (wx_id: %s, len(html_queue_data): %s)' % (wx_id, len(html_queue_data)))
                for data in html_queue_data:
                    logger.debug('ArticleUrlWorker: html_queue_data into queue start (%s, %s, %s, %s, %s)' % data)
                    self.p_queue.put(data)
                    logger.debug('ArticleUrlWorker: html_queue_data into queue end (%s, %s, %s, %s, %s)' % data)

                history_status = 1 #1代表已经完成首页的抓取工作
                #把更新weixin数据表的信息放入队列
                logger.debug('ArticleUrlWorker: put weixin_data into queue start (%s, %s, %s)' % (settings.tb_weixin, history_status, wx_id))
                self.p_queue.put((settings.tb_weixin, history_status, wx_id))
                logger.debug('ArticleUrlWorker: put weixin_data into queue end (%s, %s, %s)' % (settings.tb_weixin, history_status, wx_id))

            if history_status == 1 and isFriend == '1':
                logger.debug('ArticleUrlWorker: history_status == 1 and isFriend=1')
                # #如果是好友，则继续请求history信息
                logger.debug("ArticleUrlWorker: resolve_history requests:")
                #获取history中的msgList数据
                history_msgList = article_history.resolve_msgList(history_redirect_url, frommsgid, count=500)
                if not history_msgList:
                    logger.warning("ArticleUrlWorker: resolve_history: failed (%s, %s)" % (wx_biz, history_redirect_url))
                    continue
                logger.debug('ArticleUrlWorker: resolve_history:add_msgList_to_queue history start :%s' % len(history_msgList))
                history_queue_data = article_utils.get_article_queue_data(history_msgList, wx_id, settings.tb_article, None)
                logger.debug('ArticleUrlWorker: history_queue_data length: (wx_id: %s, len(history_queue_data): %s)' % (wx_id, len(history_queue_data)))
                for data in history_queue_data:
                    logger.debug('ArticleUrlWorker: put history_queue_data into queue start (%s, %s, %s, %s, %s)' % data)
                    self.p_queue.put(data)
                    logger.debug('ArticleUrlWorker: put history_queue_data into queue end (%s, %s, %s, %s, %s)' % data)
                history_status = 2 #2代表已经完成历史数据的抓取工作
                #把更新weixin数据表的信息放入队列
                logger.debug('ArticleUrlWorker: put weixin_data into queue start (%s, %s, %s)' % (settings.tb_weixin, history_status, wx_id))
                self.p_queue.put((settings.tb_weixin, history_status, wx_id))
                logger.debug('ArticleUrlWorker: put weixin_data into queue end (%s, %s, %s)' % (settings.tb_weixin, history_status, wx_id))
            else:
                logger.debug('ArticleUrlWorker:not fan of this wx_id, wx_biz :(%s, %s' % (wx_id, wx_biz))

            sleep_time = 5.0/len_ng_hrefs
            logger.debug('ArticleUrlWorker: sleep start...%s' % sleep_time)
            time.sleep(sleep_time)
            logger.debug('ArticleUrlWorker: sleep end...%s'% sleep_time)

            idx_ng_hrefs += 1

        logger.debug('ArticleUrlWorker: close cursor and conn')

    def get_link_ng_hrefs(self):
        link, ng_hrefs = None, []
        logger.debug('ArticleUrlWorker: get_content_ng_hrefs: Waiting for history message in browser')
        #用户发送历史消息之后确认
        order = raw_input("If you have done your history messages, Please enter 'yes':")
        if order != 'yes':
            return (None, ng_hrefs)
        #解析历史消息内容contents
        logger.debug('ArticleUrlWorker: get_content_ng_hrefs: get contents start')
        contents = self.driver.find_elements_by_css_selector('a.app')
        logger.debug('ArticleUrlWorker: get_content_ng_hrefs: get contents end:%s' % str(contents))

        if not contents:
            logger.debug('ArticleUrlWorker: get_content_ng_hrefs: contents is EMPTY!:%s' % contents)
            return (link, ng_hrefs)

        logger.debug('ArticleUrlWorker: get_content_ng_hrefs: get ng_hrefs from contents start')
        #解析contents中的ng_href值
        for content in contents:
            #从content中解析出ng_href值
            ng_href = self.resolve_ng_href(content)
            if ng_href:
                logger.debug('ArticleUrlWorker: get_content_ng_hrefs: ng_href: %s' % ng_href)
                link = content
                ng_hrefs.append(ng_href)

        if not len(ng_hrefs):
            logger.debug('ArticleUrlWorker: get_content_ng_hrefs: ng_hrefs is EMPTY!:%s' % contents)
            return (link, ng_hrefs)

        return (link, ng_hrefs)

    def switch_to_new_window(self, link):
        logger.debug('ArticleUrlWorker: switch_to_new_window start')
        link.click()
        #如果不sleep一下的话，另外一个window还没出现，switch动作就已经发生了
        time.sleep(1)
        self.driver.switch_to.window(self.driver.window_handles[-1])
        logger.debug('ArticleUrlWorker: switch_to_new_window end')

    def resolve_ng_href(self, content):
        ng_href = None
        try:
            logger.debug('ArticleUrlWorker: resolve_ng_href: ng_href start')
            ng_href = content.get_attribute('ng-href')
            logger.debug('ArticleUrlWorker: resolve_ng_href: ng_href end:%s' % str(ng_href))
        except Exception as e:
            logger.debug('ArticleUrlWorker: resolve_ng_href:%s' % e)
        return ng_href

    def get_history_redirect_url(self, ng_href, wx_biz):
        history_redirect_url = None
        try:
            tar_biz = wx_biz.replace('==', '')
            raw_req_url = 'https://wx.qq.com' + ng_href
            #注意百分比符号的替换
            if '==' in wx_biz:
                tar_req_url = re.sub(r'__biz\%\w+\%3D\%3D\%23', '__biz%3D' + tar_biz + '%3D%3D%23', raw_req_url)
            else:
                tar_req_url = re.sub(r'__biz\%\w+\%3D\%3D\%23', '__biz%3D' + tar_biz + '%23', raw_req_url)

            logger.info('ArticleUrlWorker: get_history_redirect_url:Getting req_url start:(%s)' % wx_biz)
            self.driver.get(tar_req_url)
            time.sleep(1)
            history_redirect_url = self.driver.current_url
            logger.debug('ArticleUrlWorker: get_history_redirect_url:Getting req_url end:(%s, %s)' % (wx_biz, history_redirect_url))

        except urllib2.HTTPError as e:
            logger.debug('ArticleUrlWorker: get_history_redirect_url:urllib2.HTTPError %s' % e)
        except Exception as e:
            logger.debug("ArticleUrlWorker: get_history_redirect_url: exception %s " % e)

        return history_redirect_url