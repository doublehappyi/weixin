# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
# ####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import json

def resolve_msgList(html, history_redirect_url):
    msgList = []
    r_msgList = 'msgList ='
    msg_list_str = ''
    #从html中解析出msglist数据
    lines = html.split('\n')
    for line in lines:
        if r_msgList in line:
            msg_list_str = line.strip()
            break
    if not msg_list_str:
        logger.debug("artile_html: resolve_msgList: msgList is not found :%s" % html)
        return []
    #把msglist字符串序列化成字典
    msg_list_str = msg_list_str.replace(r_msgList, '').strip().\
        replace('&quot;','"').replace('&amp;amp;', '&').replace(';','').replace('\\\\/', '/').replace("'", '')
    logger.debug('artile_html:resolve_msgList: conent_url_count, history_redirect_url(%s, %s)' % (msg_list_str.count('content_url'), history_redirect_url))
    try:
        msg_list_json = json.loads(msg_list_str)
        msgList = msg_list_json['list']
    except ValueError as e:
        msgList = []
        logger.warning('ArticleUrlWorker: resolve_msgList ValueError (%s, %s)' % (e, html))
    except KeyError as e:
        msgList = []
        logger.warning('ArticleUrlWorker: resolve_msgList KeyError (%s, %s)' % (e, html))
    except Exception as e:
        msgList = []
        logger.warning('ArticleUrlWorker: resolve_msgList failed %s' % e)

    return msgList


def resolve_isFriend(html):
    r_isFriend = 'isFriend'
    isFriendLine = ''
    lines = html.split('\n')
    for line in lines:
        if r_isFriend in line:
            isFriendLine = line.strip()
            break
    if not isFriendLine:
        logger.debug("artile_html: resolve_isFriend: isFriend is not found :%s" % html)
        return None

    try:
        isFriend = isFriendLine.replace(',', '').replace('\'', '').split('=')[1].strip()
    except IndexError as e:
        logger.debug('artile_html: resolve_isFriend: isFriend IndexError:(%s, %s)' % (e, isFriend))
        isFriend = None

    return isFriend