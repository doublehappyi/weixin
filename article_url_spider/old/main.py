# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import MySQLdb
import Queue
from article_url_spider import ArticleUrlManager
from article_url_updater import ArticleUrlUpdater

def get_biz_queue(url_spider_num):
    q = Queue.Queue()
    conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db,
                               charset='utf8')
    cursor = conn.cursor()
    cursor.execute(
        """select wx_id, wx_biz, history_status from %s where wx_biz is not null and wx_biz != '0' """ % settings.tb_weixin)
    rows = cursor.fetchall()
    for row in rows:
        logger.debug('get_biz_queue:Putting into queue start (%s, %s, %s) ' % row)
        q.put((row[0], row[1], row[2]))
        logger.debug('get_biz_queue:Putting into queue end (%s, %s, %s) ' % row)
    for i in xrange(url_spider_num):
        logger.debug('get_biz_queue:Putting into queue start None')
        q.put((None, None, None))
        logger.debug('get_biz_queue:Putting into queue end None')
    cursor.close()
    conn.close()
    return q


if __name__ == '__main__':
    url_spider_num = 1
    url_updater_num = 1
    biz_queue = get_biz_queue(url_spider_num)
    article_url_queue = Queue.Queue()

    logger.debug('Starting ArticleUrlManager')
    article_url_mgr = ArticleUrlManager(biz_queue, article_url_queue, url_spider_num, url_updater_num)
    logger.debug('Starting ArticleUrlManager OK')

    logger.debug('Starting ArticleUrlManager')
    article_updater = ArticleUrlUpdater(article_url_queue)
    logger.debug('Starting ArticleUrlManager OK')

    article_url_mgr.join()
    article_updater.join()

    logger.debug('Finished!')
