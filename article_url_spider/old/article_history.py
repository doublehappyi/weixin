# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
# ####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import json
import article_utils

def resolve_msgList(history_redirect_url, frommsgid, count=500):
    history_list = []
    base_history_url = history_redirect_url.replace('&devicetype=webwx', '').replace('#wechat_webview_type=1', '') + '&f=json&x5=0'
    is_continue = True
    while is_continue:
        msg_url = base_history_url + "&frommsgid=%s&count=%s" % (frommsgid, count)
        logger.debug('article_history: resolve_msgList msg_url: %s' % msg_url)
        r = article_utils.get_response(msg_url)
        if not r:
            logger.debug('article_history: resolve_msgList get_response failed %s' % msg_url)
            break
        try:
            logger.debug("article_history: resolve_msgList r.text ok")
            data = json.loads(r.text)
            # data = r.json()
            if data.get('ret') != 0:
                logger.debug('article_history: resolve_msgList ret failed %s' % msg_url)
                break
            general_msg_list_str = data['general_msg_list'].replace('\\\/', '/')
            logger.debug('article_history: resolve_msgList general_msg_list_str ok')
            general_msg_list = json.loads(general_msg_list_str)
            data_list = general_msg_list['list']
            logger.debug('article_history: resolve_msgList data_list ok:len(data_list):%s' % len(data_list))
            history_list.extend(data_list)
            #翻页：重置frommsgid
            is_continue = data.get('is_continue')
            frommsgid = data_list[-1].get('comm_msg_info').get('id')
            logger.debug('article_history: resolve_msgList reset frommsgid is_continue (%s, %s)' % (frommsgid, is_continue))
        except ValueError as e:
            logger.warning("article_history: resolve_msgList ValueError: (%s, %s, %s)" % (e, msg_url, len(history_list)))
            break
        except Exception as e:
            logger.warning("article_history: resolve_msgList Exception:  (%s, %s, %s)" % (e, msg_url, len(history_list)))
            break
    logger.debug('article_history:resolve_msgList: conent_url_count, msg_url(%s, %s)' % (str(history_list).count('content_url'), msg_url))
    return history_list