# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################


import threading
import urllib2
from selenium import webdriver
import re, json
import time
import requests
import random

url = 'http://mp.weixin.qq.com/s?__biz=MjM5MzIxOTYwMA==&mid=209751047&idx=4&sn=7a47623fa1a31fdf517e9fd01582a578&scene=4#wechat_redirect'
class ArticleUrlManager(object):
    def __init__(self, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(ArticleUrlWorker(self.c_queue, self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('ArticleUrlManager:Putting (None, None, None, None) into p_queue')
            self.p_queue.put((None, None, None, None))


class ArticleUrlWorker(threading.Thread):
    def __init__(self, c_queue, p_queue):
        super(ArticleUrlWorker, self).__init__()
        self.c_queue = c_queue
        self.p_queue = p_queue
        # self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome()
        self.setDaemon(True)
        self.start()

    def run(self):
        logger.debug('ArticleUrlWorker: starting get https://wx.qq.com/')
        self.driver.get('https://wx.qq.com/')
        logger.debug('ArticleUrlWorker: starting get https://wx.qq.com/ OK')
        #获取link以及ng_hrefs
        link, ng_hrefs = self.get_link_ng_hrefs()
        #判断ng_hrefs有效，否则重试获取
        if not link or not ng_hrefs:
            logger.debug('ArticleUrlWorker:ng_href error!')
            return
        #打开新的窗口
        self.switch_to_new_window(link)
        #
        idx_ng_hrefs, len_ng_hrefs = 0, len(ng_hrefs)
        while 1:
            #从ng_hrefs中轮流获取ng_href
            ng_href = ng_hrefs[idx_ng_hrefs % len_ng_hrefs]
            logger.debug('ArticleUrlWorker:ng_href : %s' % ng_href)
            #从队列中获取wx_id, wx_biz
            logger.debug('ArticleUrlWorker:get wx_id and wx_biz start')
            wx_id, wx_biz, history_status = self.c_queue.get(); self.c_queue.task_done()
            logger.debug('ArticleUrlWorker:get wx_id and wx_biz end (%s, %s)' % (wx_id, wx_biz))
            #判断队列是否将要结束
            if not wx_id:
                logger.debug('ArticleUrlWorker: no more data in c_queue')
                break
            #利用wx_biz重组ng_href，生成https请求，取回重定向的历史消息页面page_url
            page_url = self.get_page_url(ng_href, wx_biz)
            #如获取失败，则忽略该请求，循环重新执行
            if not page_url:
                logger.debug('ArticleUrlWorker:get_page_url failed wx_biz is :%s' % wx_biz)
                continue
            logger.debug('ArticleUrlWorker:get_page_url success:%s' % page_url)
            #获取页面html，如果获取失败，则忽略该wx_id，循环继续
            html = self.get_html(page_url)
            if not html:
                logger.warning("ArticleUrlWorker: get_html: failed (%s, %s)" % (wx_biz, page_url))
                continue
            #history_status有3个值：#0代表从未获取过数据,1代表获取过第一页数据,2代表已获取过历史数据
            if history_status == 0:
                #获取html中的list数据
                html_list = self.resolve_html(html)
                if not html_list:
                    logger.warning("ArticleUrlWorker: resolve_html: failed (%s, %s)" % (wx_biz, page_url))
                    continue
                logger.debu('ArticleUrlWorker: resolve_html:add_list_to_queue html start')
                self.add_list_to_queue(html_list, 1, wx_id)

                frommsgid = html_list[-1].get('comm_msg_info').get('id')
                #获取history中的数据
                history_list = self.resolve_history(page_url, frommsgid)
                if not history_list:
                    logger.warning("ArticleUrlWorker: resolve_html: failed (%s, %s)" % (wx_biz, page_url))
                    continue
                logger.debu('ArticleUrlWorker: resolve_html:add_list_to_queue history start')
                self.add_list_to_queue(html_list, 2, wx_id)
            elif history_status == 1:
                #获取history中的数据
                history_list = self.resolve_history(page_url)
                if not history_list:
                    logger.warning("ArticleUrlWorker: resolve_html: failed (%s, %s)" % (wx_biz, page_url))
                    continue
                self.add_list_to_queue(html_list, 2, wx_id)
            elif history_status == 2:
                #获取html中的list数据
                today_list = self.resolve_today(html)
                if not html_list:
                    logger.warning("ArticleUrlWorker: resolve_html: failed (%s, %s)" % (wx_biz, page_url))
                    continue
                self.add_list_to_queue(today_list, 1, wx_id)
            else:
                pass

            # self.handle_page_url(page_url, wx_id)

            sleep_time = 5.0/len_ng_hrefs
            logger.debug('ArticleUrlWorker: sleep start...%s' % sleep_time)
            time.sleep(sleep_time)
            logger.debug('ArticleUrlWorker: sleep end...%s'% sleep_time)

            idx_ng_hrefs += 1

    def get_link_ng_hrefs(self):
        link, ng_hrefs = None, []
        logger.debug('ArticleUrlWorker: get_content_ng_hrefs: Waiting for history message in browser')
        #用户发送历史消息之后确认
        order = raw_input("If you have done your history messages, Please enter 'yes':")
        if order != 'yes':
            return (None, ng_hrefs)
        #解析历史消息内容contents
        logger.debug('ArticleUrlWorker: get_content_ng_hrefs: get contents start')
        contents = self.driver.find_elements_by_css_selector('a.app')
        logger.debug('ArticleUrlWorker: get_content_ng_hrefs: get contents end:%s' % str(contents))

        if not contents:
            logger.debug('ArticleUrlWorker: get_content_ng_hrefs: contents is EMPTY!:%s' % contents)
            return (link, ng_hrefs)

        logger.debug('ArticleUrlWorker: get_content_ng_hrefs: get ng_hrefs from contents start')
        #解析contents中的ng_href值
        for content in contents:
            #从content中解析出ng_href值
            ng_href = self.resolve_ng_href(content)
            if ng_href:
                logger.debug('ArticleUrlWorker: get_content_ng_hrefs: ng_href: %s' % ng_href)
                link = content
                ng_hrefs.append(ng_href)

        if not len(ng_hrefs):
            logger.debug('ArticleUrlWorker: get_content_ng_hrefs: ng_hrefs is EMPTY!:%s' % contents)
            return (link, ng_hrefs)

        return (link, ng_hrefs)

    def switch_to_new_window(self, link):
        logger.debug('ArticleUrlWorker: switch_to_new_window start')
        link.click()
        #如果不sleep一下的话，另外一个window还没出现，switch动作就已经发生了
        time.sleep(1)
        self.driver.switch_to.window(self.driver.window_handles[-1])
        logger.debug('ArticleUrlWorker: switch_to_new_window end')

    def resolve_ng_href(self, content):
        ng_href = None
        try:
            logger.debug('ArticleUrlWorker: resolve_ng_href: ng_href start')
            ng_href = content.get_attribute('ng-href')
            logger.debug('ArticleUrlWorker: resolve_ng_href: ng_href end:%s' % str(ng_href))
        except Exception, e:
            logger.debug('ArticleUrlWorker: resolve_ng_href:NoSuchElementException')
        return ng_href

    def get_page_url(self, ng_href, wx_biz):
        page_url = None
        try:
            tar_biz = wx_biz.replace('==', '')
            raw_req_url = 'https://wx2.qq.com' + ng_href
            #注意百分比符号的替换
            if '==' in wx_biz:
                tar_req_url = re.sub(r'__biz\%\w+\%3D\%3D\%23', '__biz%3D' + tar_biz + '%3D%3D%23', raw_req_url)
            else:
                tar_req_url = re.sub(r'__biz\%\w+\%3D\%3D\%23', '__biz%3D' + tar_biz + '%23', raw_req_url)

            logger.info('ArticleUrlWorker: get_page_url:Getting req_url start:(%s, %s)' % (wx_biz, page_url))
            self.driver.get(tar_req_url)
            page_url = self.driver.current_url
            logger.debug('ArticleUrlWorker: get_page_url:Getting req_url end:(%s, %s)' % (wx_biz, page_url))

        except urllib2.HTTPError, e:
            logger.debug('ArticleUrlWorker: get_page_url:urllib2.HTTPError')
        except Exception, e:
            logger.debug("ArticleUrlWorker: get_page_url: exception %s " % e)

        return page_url

    def handle_page_url(self, page_url, wx_id):
        r_msgList = 'msgList ='
        retry = 0
        html = None
        while 1:
            try:
                html = urllib2.urlopen(page_url).read()
                break
            except KeyError, e:
                logger.warning("ArticleUrlWorker: handle_page_url: %s" % e)
            except urllib2.HTTPError, e:
                logger.warning("ArticleUrlWorker: urllib2.HTTPError: %s" % e)
                retry += 1
                if retry > settings.max_retry:
                    break
                logger.warning("ArticleUrlWorker: Retry times: %s" % retry)
            except Exception, e:
                logger.warning("ArticleUrlWorker: Exception: %s" % e)

        #如果html获取失败，则退出函数
        if not html:
            return

        #从html中解析出msglist数据
        lines = html.split('\n')
        for line in lines:
            if r_msgList in line:
                msg_list_str = line.strip()
                break

        #把msglist字符串序列化成字典
        msg_list_str = msg_list_str.replace(r_msgList, '').strip().\
            replace('&quot;','"').replace('amp;', '').replace(';','').replace('\\\\/', '/').replace("'", '')
        msg_list_json = json.loads(msg_list_str)
        pages_list = msg_list_json['list']

        #判断pages_list数据长度
        if not len(pages_list):
            logger.debug('ArticleUrlWorker: handle_page_url:page_list len 0')
            return

        for page in pages_list:
            #获取发表日期
            article_publish_time = page.get('comm_msg_info', {}).get('datetime', None)
            #判断是否有key:app_msg_ext_info
            if 'app_msg_ext_info' not in page:
                logger.debug('ArticleUrlWorker: handle_page_url: app_msg_ext_info is not exist')
                return
            #处理外层带图片的文章
            app_msg_ext_info = page['app_msg_ext_info']
            logger.debug('ArticleUrlWorker: handle_page_url: Putting content_url from app_msg_ext_info: %s' % app_msg_ext_info['content_url'])
            article_title = app_msg_ext_info['title'].replace('&nbsp', '')
            article_url = app_msg_ext_info['content_url']
            logger.debug('ArticleUrlWorker: handle_page_url: put into queue from app_msg_ext_info: (%s, %s, %s, %s)' %
                         (article_title, article_publish_time, article_url, wx_id))
            self.p_queue.put((article_title, article_url, article_publish_time, wx_id))

            #判断是否有key:multi_app_msg_item_list
            if 'multi_app_msg_item_list' not in app_msg_ext_info:
                logger.debug('ArticleUrlWorker: handle_page_url: multi_app_msg_item_list is not exist')
                return

            #获取嵌套内层文章lists
            multi_app_msg_item_list = app_msg_ext_info['multi_app_msg_item_list']

            #判断multi_app_msg_item_list长度是否为0：
            if not len(multi_app_msg_item_list):
                logger.debug('ArticleUrlWorker: handle_page_url: multi_app_msg_item_list len = 0')
                return

            #处理内层文章列表
            for app_msg_item in multi_app_msg_item_list:
                article_title = app_msg_item['title'].replace('&nbsp', '')
                article_url = app_msg_item['content_url']
                logger.debug('ArticleUrlWorker: handle_page_url: put into queue from app_msg_item: (%s, %s, %s, %s)' %
                             (article_title, article_publish_time, article_url, wx_id))
                self.p_queue.put((article_title, article_url, article_publish_time, wx_id))
    def get_html(self, page_url):
        retry = 0
        html = None
        while 1:
            try:
                html = urllib2.urlopen(page_url).read()
                break
            except KeyError, e:
                logger.warning("ArticleUrlWorker: get_html: %s" % e)
            except urllib2.HTTPError, e:
                logger.warning("ArticleUrlWorker: get_html: urllib2.HTTPError: %s" % e)
                retry += 1
                if retry > settings.max_retry:
                    break
                logger.warning("ArticleUrlWorker: get_html: Retry times: %s" % retry)
            except Exception, e:
                logger.warning("ArticleUrlWorker: get_html: Exception: %s" % e)

        return html
    def get_data_from_list(self, data_lists, wx_id):
        data = []
        #判断pages_list数据长度
        if not len(data_lists):
            logger.debug('ArticleUrlWorker: handle_page_url:page_list len 0')
            return

        for page in data_lists:
            #获取发表日期
            article_publish_time = page.get('comm_msg_info', {}).get('datetime', None)
            #判断是否有key:app_msg_ext_info
            if 'app_msg_ext_info' not in page:
                logger.debug('ArticleUrlWorker: handle_page_url: app_msg_ext_info is not exist')
                return
            #处理外层带图片的文章
            app_msg_ext_info = page['app_msg_ext_info']
            logger.debug('ArticleUrlWorker: handle_page_url: Putting content_url from app_msg_ext_info: %s' % app_msg_ext_info['content_url'])
            article_title = app_msg_ext_info['title'].replace('&nbsp', '')
            article_url = app_msg_ext_info['content_url']
            logger.debug('ArticleUrlWorker: handle_page_url: put into queue from app_msg_ext_info: (%s, %s, %s, %s)' %
                         (article_title, article_publish_time, article_url))
            data.append((article_title, article_url, article_publish_time))
            # self.p_queue.put((article_title, article_url, article_publish_time, wx_id))

            #判断是否有key:multi_app_msg_item_list
            if 'multi_app_msg_item_list' not in app_msg_ext_info:
                logger.debug('ArticleUrlWorker: handle_page_url: multi_app_msg_item_list is not exist')
                return

            #获取嵌套内层文章lists
            multi_app_msg_item_list = app_msg_ext_info['multi_app_msg_item_list']

            #判断multi_app_msg_item_list长度是否为0：
            if not len(multi_app_msg_item_list):
                logger.debug('ArticleUrlWorker: handle_page_url: multi_app_msg_item_list len = 0')
                return

            #处理内层文章列表
            for app_msg_item in multi_app_msg_item_list:
                article_title = app_msg_item['title'].replace('&nbsp', '')
                article_url = app_msg_item['content_url']
                logger.debug('ArticleUrlWorker: handle_page_url: put into queue from app_msg_item: (%s, %s, %s, %s)' %
                             (article_title, article_publish_time, article_url, wx_id))
                # self.p_queue.put((article_title, article_url, article_publish_time, wx_id))
                data.append((article_title, article_url, article_publish_time, wx_id))
    def resolve_html(self, html):
        html_list = []
        r_msgList = 'msgList ='
        #从html中解析出msglist数据
        lines = html.split('\n')
        for line in lines:
            if r_msgList in line:
                msg_list_str = line.strip()
                break
        #把msglist字符串序列化成字典
        msg_list_str = msg_list_str.replace(r_msgList, '').strip().\
            replace('&quot;','"').replace('&amp;amp;', '&').replace(';','').replace('\\\\/', '/').replace("'", '')
        try:
            msg_list_json = json.loads(msg_list_str)
            html_list = msg_list_json['list']
        except Exception as e:
            logger.warning('ArticleUrlWorker: resolve_html failed %s' % e)

        return html_list

    def get_history(self, msg_url):
        r = None
        retry = 0
        while 1:
            try:
                r = requests.get(msg_url)
                if r.status_code != 200:
                    logger.debug('ArticleUrlWorker: resolve_history failed %s' % msg_url)
                    continue
            except requests.exceptions.Timeout as e:
                logger.warning('ArticleUrlWorker: resolve_history requests.exceptions.Timeout %s' % e)
                retry +=1
                if retry > settings.max_retry:
                    break
                logger.warning('ArticleUrlWorker: resolve_history retry times %s' % retry)
                continue
            except requests.exceptions.HTTPError as e:
                logger.warning('ArticleUrlWorker: resolve_history requests.exceptions.HTTPError %s' % e)
                retry +=1
                if retry > settings.max_retry:
                    break
                logger.warning('ArticleUrlWorker: resolve_history retry times %s' % retry)
                continue
            except requests.exceptions.RequestException as e:
                logger.warning('ArticleUrlWorker: resolve_history requests.exceptions.RequestException %s' % e)
                retry +=1
                if retry > settings.max_retry:
                    break
                logger.warning('ArticleUrlWorker: resolve_history retry times %s' % retry)
                continue
            except Exception as e:
                logger.warning('ArticleUrlWorker: resolve_history Exception %s' % e)
                continue
        return r

    def resolve_history(self, page_url, frommsgid, count=500):
        history_list = []
        base_history_url = page_url.replace('&devicetype=webwx', '') + 'f=json&x5=0'
        is_continue = True
        while is_continue:
            msg_url = base_history_url + "&frommsgid=%s&count=%s" % (frommsgid, count)
            r = self.get_history(msg_url)
            if not r:
                continue
            try:
                # data = json.loads(r.text)
                data = r.json()
                if data.get('ret') != 0:
                    logger.debug('ArticleUrlWorker: resolve_history failed %s' % msg_url)
                    continue
                logger.debug('get... list')
                data_list = data.get('general_msg_list').get('list')
                logger.debug('get... list ok')
                history_list.extend(data_list)
                #翻页
                frommsgid = data_list.get('comm_msg_info').get('id')
                is_continue = data.get('is_continue')
                logger.debug('ArticleUrlWorker: resolve_history reset frommsgid is_continue (%s, %s)' % (frommsgid, is_continue))
            except ValueError as e:
                logger.warning("ArticleUrlWorker: resolve_history ValueError: %s" % e)
            except Exception as e:
                logger.warning("ArticleUrlWorker: resolve_history Exception: %s" % e)

        return history_list
        # msg_url = page_url + 'f=json&frommsgid=%s&count=%s&x5=0' % (frommsgid, count)
        # msg_url = 'http://mp.weixin.qq.com/mp/getmasssendmsg?__biz=MzA5ODM5MDU3MA==&uin=MTcwMDEyNDEzMA==&key=dffc561732c22651ccc0fc0e5d6074c5e3898f179be86cc49ee37e5666a4288b782b0ad5222458a3d13129eefa4e2bbe&f=json&frommsgid=212496373&count=10&uin=MTcwMDEyNDEzMA%3D%3D&key=dffc561732c22651ccc0fc0e5d6074c5e3898f179be86cc49ee37e5666a4288b782b0ad5222458a3d13129eefa4e2bbe&pass_ticket=DeeNGrYbdc2Q9Qq6zr9aAYPDM%25252F8LJWz7a133Ukqp%25252BQS2taBcUyerABJNFnOUlb%25252Bl&x5=0'
        # msg_url = 'f=json&frommsgid=212496373&count=10&x5=0'
        # htm_url = 'devicetype=webwx&version=70000001&lang=zh_CN#wechat_webview_type=1'
        # html_url = 'http://mp.weixin.qq.com/mp/getmasssendmsg?__biz=MzA5ODM5MDU3MA==&uin=MTcwMDEyNDEzMA%3D%3D&key=dffc561732c226512245f119ec87449f3c586829aa3c724d5dd9aecd0e0c80417d101d4a7a650a9a5d002ac3cb0fc0e7&f=json&frommsgid=212496373&count=10&x5=0&version=70000001&lang=zh_CN&pass_ticket=DeeNGrYbdc2Q9Qq6zr9aAYPDM%2F8LJWz7a133Ukqp%2BQS2taBcUyerABJNFnOUlb%2Bl#wechat_webview_type=1'
        # html_url = 'http://mp.weixin.qq.com/mp/getmasssendmsg?__biz=MzA5ODM5MDU3MA==&uin=MTcwMDEyNDEzMA%3D%3D&key=dffc561732c22651402f3b94e2171364bc79b17dede02cbdf388f74974207ac24c4a2c8d2f74ffebe699ec677fb884c5&lang=zh_CN&pass_ticket=DeeNGrYbdc2Q9Qq6zr9aAYPDM%2F8LJWz7a133Ukqp%2BQS2taBcUyerABJNFnOUlb%2Bl&f=json&frommsgid=212496373&count=10&x5=0'

    def resolve_today(self):
        pass

    def add_list_to_queue(self, pages_list, history_status, wx_id):
        for page in pages_list:
            #获取发表日期
            article_publish_time = page.get('comm_msg_info', {}).get('datetime', None)
            #判断是否有key:app_msg_ext_info
            if 'app_msg_ext_info' not in page:
                logger.debug('ArticleUrlWorker: add_list_to_queue: app_msg_ext_info is not exist')
                return
            #处理外层带图片的文章
            app_msg_ext_info = page['app_msg_ext_info']
            logger.debug('ArticleUrlWorker: add_list_to_queue: Putting content_url from app_msg_ext_info: %s' % app_msg_ext_info['content_url'])
            article_title = app_msg_ext_info['title'].replace('&nbsp', '')
            article_url = app_msg_ext_info['content_url']
            logger.debug('ArticleUrlWorker: add_list_to_queue: put into queue from app_msg_ext_info: (%s, %s, %s, %s)' %
                         (article_title, article_publish_time, article_url, wx_id))
            self.p_queue.put((article_title, article_url, article_publish_time, wx_id))

            #判断是否有key:multi_app_msg_item_list
            if 'multi_app_msg_item_list' not in app_msg_ext_info:
                logger.debug('ArticleUrlWorker: add_list_to_queue: multi_app_msg_item_list is not exist')
                return

            #获取嵌套内层文章lists
            multi_app_msg_item_list = app_msg_ext_info['multi_app_msg_item_list']

            #判断multi_app_msg_item_list长度是否为0：
            if not len(multi_app_msg_item_list):
                logger.debug('ArticleUrlWorker: add_list_to_queue: multi_app_msg_item_list len = 0')
                return

            #处理内层文章列表
            for app_msg_item in multi_app_msg_item_list:
                article_title = app_msg_item['title'].replace('&nbsp', '')
                article_url = app_msg_item['content_url']
                logger.debug('ArticleUrlWorker: add_list_to_queue: put into queue from app_msg_item: (%s, %s, %s, %s)' %
                             (article_title, article_publish_time, article_url, wx_id))
                self.p_queue.put((article_title, article_url, article_publish_time, wx_id))
