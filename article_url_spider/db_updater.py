# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
import time


class DBUpdaterManager(threading.Thread):
    def __init__(self, c_queue):
        super(DBUpdaterManager, self).__init__()
        self.c_queue = c_queue
        self.conn = settings.getConnection()

        self.cursor = self.conn.cursor()
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            try:
                #从队列里面取数据
                logger.debug("DBUpdaterManager: Getting data from c_queue start")
                task = self.c_queue.get();self.c_queue.task_done()
                logger.debug("DBUpdaterManager: Getting data from c_queue end {0}".format(str(task)))
                table = task[0]
                logger.debug('DBUpdaterManager:table: {0}'.format(table))
                if not table:
                    logger.debug('DBUpdaterManager: no more data in c_queue')
                    break

                if table == settings.db['table']['article']:
                    self.update_article(task)
                elif table == settings.db['table']['weixin']:
                    self.update_weixin(task)
                else:
                    logger.debug('DBUpdaterManager: wrong table')
            except Exception as e:
                logger.error("Exception in DBUpdaterManager: E: {0}, task:{1}".format(e, str(task)))

        logger.debug('closing db conn, cursor')
        self.cursor.close()
        self.conn.close()
        logger.debug('closing db conn, cursor OK')

    def update_weixin(self, task):
        try:
            #从队列里面取数据
            logger.debug("DBUpdaterManager: update_weixin: Getting data from c_queue start")
            table, wx_id, history_status = task
            logger.debug("DBUpdaterManager: update_weixin: Getting data from c_queue end {0}".format(str(task)))

            #更新到数据库
            update_time = int(time.time())
            logger.debug('DBUpdaterManager: update_weixin: update table start {0}'.format(str(task)))
            sql_str = "update {0} t set t.history_status={1}, t.update_time={2} where wx_id={3}".format(table, history_status, update_time, wx_id)
            self.cursor.execute(sql_str)
            self.conn.commit()
            logger.debug('DBUpdaterManager: update_weixin: update table end: {0}'.format(str(task)))

        except Exception,e:
            logger.error("DBUpdaterManager:update_weixin: Exception: E: {0}, task: ({1})".format(e, str(task)))
            time.sleep(5)

    def update_article(self, task):
        try:
            #从队列里面取数据
            logger.debug("DBUpdaterManager: update_article: Getting data from c_queue start")
            table, article_title, article_url, article_publish_time, ref_wx_id = task
            logger.debug("DBUpdaterManager: update_article: Getting data from c_queue end ({0})".format(str(task)))

            #查询数据库是否已经有对应的文章链接了，如果有了，就不再做插入操作。
            # select_str = """select article_id from {0} where """.format(table)
            #
            # logger.debug("DBUpdaterManager: update_article: select_str({0})".format(select_str))
            #
            # filter_str = """article_url=%s"""
            #
            # logger.debug("DBUpdaterManager: update_article: filter_str({0})".format(filter_str))
            #
            # select_sql_str = select_str + filter_str
            #
            # logger.debug("DBUpdaterManager: update_article: select_sql_str({0})".format(select_sql_str))

            select_sql_str = """select article_id from {0} where article_url='{1}'""".format(table, article_url)
            logger.debug("DBUpdaterManager: update_article: select_sql_str({0})".format(select_sql_str))
            self.cursor.execute(select_sql_str)

            logger.debug("DBUpdaterManager: update_article: execute({0})".format(select_sql_str))

            rows = self.cursor.fetchall()
            if rows:
                logger.debug('DBUpdaterManager: update_article: duplicated entry :({0}, {1})'.format(rows[0], article_url))
                return

            #更新到数据库
            update_time = int(time.time())
            logger.debug('DBUpdaterManager: update_article: insert into table start {0}'.format(str(task)))
            insert_str = """insert into {0} """.format(table)
            values_str = """values (null, %s, %s, null, null, %s, %s, %s)"""
            sql_str = insert_str + values_str
            self.cursor.execute(sql_str, (article_title, article_url, article_publish_time, update_time, ref_wx_id))
            self.conn.commit()
            logger.debug('DBUpdaterManager: update_article: insert into table end {0}'.format(str(task)))

        except Exception,e:
            logger.error("DBUpdaterManager: update_article: Exception: E: {0}, task:{1}".format(e, str(task)))
            time.sleep(5)