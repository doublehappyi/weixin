# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
# ####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
# ####################日志设置#####################
import threading
import urlparse
import spider_utils
import time


class ArticleHistorySpiderManager(object):
    def __init__(self, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(ArticleHistorySpiderWorker(self.c_queue, self.p_queue, settings.config[i]))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('ArticleHistorySpiderWorker:Putting (None, ) into p_queue')
            self.p_queue.put((None,))


class ArticleHistorySpiderWorker(threading.Thread):
    def __init__(self, c_queue, p_queue, config):
        super(ArticleHistorySpiderWorker, self).__init__()
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.config = config
        self.conn = settings.getConnection()
        self.cursor = self.conn.cursor()
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            #1,先获取wx_id, wx_biz, history_status
            logger.debug(self.getName()+'ArticleHistorySpiderWorker:get data from c_queue start')
            row = self.c_queue.get()
            self.c_queue.task_done()
            logger.debug(self.getName()+'ArticleHistorySpiderWorker:get data from c_queue end: row:{0}'.format(str(row)))

            if not row[0]:
                logger.debug(self.getName()+'ArticleHistorySpiderWorker:no more data in c_queue')
                break
            wx_id, wx_biz, history_status = row
            #2,拼接处历史消息页面的https链接：可分析网页版微信得出拼接方法
            article_html_url = spider_utils.get_article_html_url(wx_biz, self.config)
            logger.debug(self.getName()+'ArticleHistorySpiderWorker:article_html_url: {0}'.format(article_html_url))
            if not article_html_url:
                logger.warning(self.getName()+'ArticleHistorySpiderWorker:get_article_html_url error:wx_id: {0}'.format(wx_id))
                continue

            #3,根据历史消息页面的https链接，获取历史消息页面内容和重定向之后的链接地址：该地址包含了key等值，可进一步用于翻页历史消息的获取
            html, current_url = spider_utils.get_html_and_url(article_html_url, self.config)
            logger.debug(self.getName()+'ArticleHistorySpiderWorker:current_url: {0}'.format(current_url))
            if not html:
                logger.warning(self.getName()+'ArticleHistorySpiderWorker:get_html error:article_html_url: {0}'.format(article_html_url))
                continue

            #4,解析历史消息页面内容的msgList
            msgList = spider_utils.getHtmlMsgList(html)
            if not msgList:
                logger.warning(self.getName()+'ArticleHistorySpiderWorker:getHtmlMsgList error:article_html_url: {0}'.format(article_html_url))
                continue

            #5根据history_status的值，把msgList放入队列
            #history_status==None：从来没有获取过任何内容；history_status==1，已经获取过首页内容；history_status==2：已经获取过历史消息；
            if history_status ==1 or history_status == 2:
                # 过滤出今天的msgList数据
                # 注意：只取1天前（1天=1*24*60*60秒）的数据
                start_datetime = int(time.time()) - 1*24*60*60
                msgList = spider_utils.get_msgList(msgList, start_datetime)
            else:
                #重置history_status状态，更新weixin数据表history_status字段
                history_status = 1
                self.p_queue.put((settings.db['table']['weixin'], wx_id, history_status))
            #把msgList放入队列
            spider_utils.put_msgList_into_queue(msgList, self.p_queue, wx_id)

            #6，根据参数history_status，判断是否已经获取过历史消息，已经获取过了则继续下一个循环，否则继续
            if history_status == 2:
                logger.debug(self.getName() + 'ArticleHistorySpiderWorker:history_status is 2')
                continue

            #7,解析历史消息页面内容的isFriend参数，确认是否能继续下一步
            isFriend = spider_utils.isFriend(html)
            logger.debug(self.getName() + 'ArticleHistorySpiderWorker:isFriend: {0}'.format(isFriend))
            if not isFriend:
                logger.debug(self.getName() + 'ArticleHistorySpiderWorker:isFriend error: {0}'.format(isFriend))
                continue

            #8，构建历史翻页数据url,并抓取msgList内容：这里只抓取历史前500条数据：
            # 这里微信有限制，一次抓取太多（比如1000）会导致数据返回不全
            key = urlparse.parse_qs(urlparse.urlparse(current_url).query)['key'][0]
            pass_ticket = urlparse.parse_qs(urlparse.urlparse(current_url).query)['pass_ticket'][0]
            biz = wx_biz
            frommsgid = msgList[-1].get('comm_msg_info').get('id')
            count = 500
            article_history_url = spider_utils.get_article_history_url(key, pass_ticket, biz, frommsgid, count)

            #9，抓取历史数据
            history_msgList = spider_utils.get_history_msgList(article_history_url)
            if not history_msgList:
                logger.debug(self.getName() + 'ArticleHistorySpiderWorker: history_msgList failed')
                continue

            #10，把历史数据history_msgList放入队列,本轮抓取工作完成
            spider_utils.put_msgList_into_queue(history_msgList, self.p_queue, wx_id)

        self.conn.close()
        self.cursor.close()
        logger.warning(self.getName()+'ArticleHistorySpiderWorker: conn and cursor closed !')






