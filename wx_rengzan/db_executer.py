# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
import time

class DbExecuter(threading.Thread):
    def __init__(self, db_queue):
        super(DbExecuter, self).__init__()
        self.db_queue = db_queue
        self.conn = settings.getConnection()
        self.cursor = self.conn.cursor()
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            try:
                logger.debug("DbExecuter: Getting data from db_queue ...")
                row = self.db_queue.get()
                self.db_queue.task_done()
                logger.debug("DbExecuter: Getting data from db_queue OK...")
                if row[0]:
                    #如果已经有该账号了，就不要再继续执行插入操作了。
                    wx_account = row[1]
                    select_str = "select wx_id from {0}".format(settings.tb_weixin) + " where wx_account=%s"
                    if self.cursor.execute(select_str, (wx_account,)):
                        logger.debug('DbExecuter: duplicate data wx_account:{0}'.format(wx_account));
                        continue

                    logger.debug('inserting into start {0}'.format(str(row)))
                    add_time = int(time.time())
                    update_time = add_time
                    insert_str = "insert into {0}".format(settings.tb_weixin) +  " values (null, %s, %s, null, %s, null, null, {0}, {1})".format(add_time, update_time)
                    self.cursor.execute(insert_str, row)
                    self.conn.commit()
                    logger.debug('inserting into end {0}'.format(str(row)))
                else:
                    logger.debug('DbExecuter: no more data in db_queue')
                    break

            except Exception,e:
                logger.error("Exception in DbExecuter: {0}".format(e))

        logger.debug('closing db conn, cursor')
        self.cursor.close()
        self.conn.close()
        logger.debug('closing db conn, cursor OK')


            



