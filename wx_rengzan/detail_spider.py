# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
from bs4 import BeautifulSoup
import time
import requests


class DetailManager(object):
    def __init__(self, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(DetailWorker(self.c_queue, self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('DetailManager:Putting (None, None) into p_queue')
            self.p_queue.put((None,))


class DetailWorker(threading.Thread):
    def __init__(self, c_queue, p_queue):
        super(DetailWorker, self).__init__()
        self.base_url = 'http://www.rengzan.com/'
        self.c_queue = c_queue
        self.p_queue = p_queue
        
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            try:
                logger.debug("DetailWorker: Getting href from c_queue start...")
                href = self.c_queue.get()
                self.c_queue.task_done()
                logger.debug("DetailWorker: Getting href from c_queue end...")

                if href:
                    logger.debug('DetailWorker: Getting  html from href start {0}'.format(href))
                    html = requests.get(self.base_url + href).text
                    logger.debug('DetailWorker: Getting  html from href end  {0}'.format(href))

                    bs = BeautifulSoup(html, 'html.parser')
                    items = bs.select('div.cont.l ul li')
                    wx_name = items[0].get_text().split()[1].strip()
                    wx_account = items[1].get_text().split()[1].strip()
                    # wx_intro = bs.select('div.scroll>dd>p')[0].get_text().strip()
                    if bs.select('.scroll dd p'):
                        logger.debug('DetailWorker: scroll p {0}'.format(href))
                        wx_intro = bs.select('.scroll dd p')[0].get_text().strip()
                    elif bs.select('.scroll dd'):
                        logger.debug('DetailWorker: scroll dd {0}'.format(href))
                        wx_intro = bs.select('.scroll dd')[0].get_text().strip()
                    else:
                        logger.debug('DetailWorker: scroll error {0}'.format(href))
                        wx_intro = None

                    if wx_name and wx_account:
                        logger.debug("DetailWorker: Putting ({0}, {1}, {2}) into p_queue".format(wx_name, wx_account, wx_intro))
                        self.p_queue.put((wx_name, wx_account, wx_intro))
                    else:
                        logger.warning("DetailWorker: Illegal data ({0}, {1}, {2})".format(wx_name, wx_account, wx_intro))
                else:
                    logger.debug('DetailWorker: no more task in c_queue')
                    break
            except requests.HTTPError, e:
                logger.error("requests.HTTPError in DetailWorker: {0}".format(e))
            except requests.ConnectionError, e:
                logger.error("requests.ConnectionError in DetailWorker: {0}".format(e))
                time.sleep(10)
            except Exception, e:
                logger.error("Exception in DetailWorker: href:{0},E{1}".format(e, href))



            



