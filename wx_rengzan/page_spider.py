# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
import requests
from bs4 import BeautifulSoup
import time


class PageManager(object):
    def __init__(self, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(PageWorker(self.c_queue, self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('PageManager:Putting None into p_queue')
            self.p_queue.put(None)

class PageWorker(threading.Thread):
    def __init__(self, c_queue, p_queue):
        super(PageWorker, self).__init__()
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            try:
                logger.debug("Getting data from c_queue start...")
                url = self.c_queue.get()
                self.c_queue.task_done()
                logger.debug("Getting data from c_queue end...")

                if url:
                    logger.debug("Getting html from url {0} start".format(url))
                    html = requests.get(url).text
                    logger.debug("Getting html from url {0} end".format(url))

                    bs = BeautifulSoup(html, 'html.parser')
                    items = bs.select('div.p-meta-title a')
                    for item in items:
                        href = item.get('href')
                        if href:
                            logger.debug('Putting href:{0} to p_queue ...'.format(href))
                            while self.p_queue.full():
                                logger.debug('PageWorker: p_queue is full, sleep 10 secs')
                                time.sleep(10)
                            self.p_queue.put(href)
                        else:
                            logger.warning("PageWorker: Illegal data")
                else:
                    logger.debug("PageWorker: no more data in c_queue")
                    break
            except requests.HTTPError, e:
                logger.error("PageWorker: requests.HTTPError in url:{0},E:{1}".format(url, e))
            except requests.ConnectionError, e:
                logger.error("PageWorker: requests.ConnectionError url:{0},E:{1}".format(url, e))
                time.sleep(10)
            except Exception,e:
                logger.error("Exception in PageWorker: url:{0},E:{1}".format(e, url))


            



