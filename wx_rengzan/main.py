# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import Queue
from bs4 import BeautifulSoup
from page_spider import PageManager
from detail_spider import DetailManager
from db_executer import DbExecuter
import requests

def init_page_queue(page_thread_num):
    page_queue = Queue.Queue()
    try:
        url = 'http://www.rengzan.com/weixin-index-id-53.html'
        html = requests.get(url).text
        bs = BeautifulSoup(html, 'html.parser')
        text = bs.select('.pagewx')[0].get_text()
        page_count = int(text.split('/')[1].strip()[:-1])
        logger.info('page_count:{0}'.format(page_count))
        for i in xrange(1, page_count + 1):
            page_queue.put("http://www.rengzan.com/weixin-index-id-53-p-{0}.html".format(i))
        for i in xrange(page_thread_num):
            page_queue.put(None)
        return page_queue
    except Exception, e:
        logger.error('Exception in init_page_queue:{0}'.format(e))
        return None

if __name__ == '__main__':
    page_thread_num = 4
    detail_thread_num = page_thread_num*20
    
    page_queue = init_page_queue(page_thread_num)
    detail_queue = Queue.Queue(500)
    db_queue = Queue.Queue()
    

    logger.debug('starting PageManager ...')
    page_mgr = PageManager(c_queue=page_queue, p_queue=detail_queue, c_thread_num=page_thread_num, p_thread_num=detail_thread_num)
    logger.debug('starting PageManager OK...')

    logger.debug('starting DetailManager ...')
    detail_mgr = DetailManager(detail_queue, db_queue, c_thread_num=detail_thread_num, p_thread_num=1)
    logger.debug('starting DetailManager OK...')

    logger.debug('starting DbExcuter ...')
    db_exe = DbExecuter(db_queue)
    logger.debug('starting DbExcuter OK ...')


    page_mgr.join()
    logger.debug('Ending page_mgr ok...')
    detail_mgr.join()
    logger.debug('Ending detail_mgr ok...')
    db_exe.join()
    logger.debug('Ending db_exe ok...')