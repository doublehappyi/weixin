#coding:utf-8
__author__ = 'yishuangxi'
import MySQLdb
# host='127.0.0.1'
# user='root'
# passwd='11111111'
# db='wx'
# tb_weixin='weixin'
# tb_article='article'

db = {
    'host':'127.0.0.1',
    'user':'root',
    'password':'11111111',
    'database':'wx',
    'table':{
        'weixin':'weixin',
        'article':'article'
    }
}

# db = {
#     'host':'172.16.188.42',
#     'user':'search',
#     'password':'search',
#     'database':'c2c_jujingpin_db',
#     'table':{
#         'weixin':'c2c_community_weixin',
#         'article':'c2c_community_article'
#     }
# }

def getConnection():
    return MySQLdb.connect(host=db['host'], user=db['user'], passwd=db['password'], db=db['database'],charset="utf8")

def get_logger(log_path):
    import os, logging, time
    log_file_name = time.strftime("%Y%m%d-%H-%M", time.localtime())
    log_format = "%(asctime)s: %(levelname)s: %(message)s"
    if not os.path.exists(log_path):
        os.mkdir(log_path)

    logging.basicConfig(filename=log_path + '/' + log_file_name + '.log', filemode='w', level=logging.DEBUG,
                        format=log_format)
    logger = logging.getLogger(__file__)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger

config = [{
    'cookie':'pgv_pvi=5279265792; pgv_si=s8585926656; webwxuvid=bcd0fe5aa5be608b63ba30a1beb595a6bc723e18fdd72585175876b63ef796540e2df2419b87cf4debcc797852d13b69; mm_lang=zh_CN; MM_WX_NOTIFY_STATE=1; MM_WX_SOUND_STATE=1; wxloadtime=1446791180_expired; wxpluginkey=1446771951; wxuin=1700124130; wxsid=PZ1o0QCWRVyY4YQo; webwx_data_ticket=AQYUG/Tn+5imqJ8arSXGM59N',
    'base_url':'https://wx2.qq.com',
    'key':'skey=%40crypt_9fd05067_3a523fd8c4e8364acee90ec74540b120&deviceid=e791552124079317&pass_ticket=undefined&opcode=2&scene=1&username=@43d3c59a7fb5b0c4f7a31d4556f1470eda557e5a1b1b2821019e7be00a3b0517'
}]