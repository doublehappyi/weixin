# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
import urllib2
from bs4 import BeautifulSoup
import urlparse
import time
import requests


class SearchManager(object):
    def __init__(self, c_queue, db_getter_queue, p_queue, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.db_getter_queue = db_getter_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(SearchWorker(self.c_queue, self.db_getter_queue, self.p_queue, ))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('SearchManager:Putting (None, None) into p_queue')
            self.p_queue.put((None, None))


class SearchWorker(threading.Thread):
    def __init__(self, c_queue, db_getter_queue, p_queue):
        super(SearchWorker, self).__init__()
        self.c_queue = c_queue
        self.db_getter_queue = db_getter_queue
        self.p_queue = p_queue
        self.setDaemon(True)
        self.start()

    def run(self):
        cookie, wx_id = None, None
        is_html_obtain = False
        while 1:
            try:
                #判断wx_id是否未初始化，或者是否已失效：若否，则从队列里面取出数据
                if wx_id is None:
                    logger.debug('SearchWorker:Getting data from db_getter_queue start')
                    wx_id, wx_name, wx_account = self.db_getter_queue.get()
                    self.db_getter_queue.task_done()
                    logger.debug('SearchWorker:Getting data from db_getter_queue end (%s, %s, %s)' % (wx_id, wx_name, wx_account))

                #判断cookie是否未初始化,或者是否已经失效
                if cookie is None:
                    logger.debug('SearchWorker:Getting data from cookie_queue start')
                    cookie = self.c_queue.get()
                    self.c_queue.task_done()
                    logger.debug('SearchWorker:Getting data from cookie_queue end %s' % cookie)

                #判断cookie和wx_id是否已经消费完，若任何一个被消费完，则循环结束
                if cookie is None:
                    logger.debug('SearchWorker:cookie is None')
                    break
                elif wx_id is None:
                    logger.debug('SearchWorker:wx_id is None')
                    break

                #判断search页面是否已经获取成功：若否，则获取search页面html
                #这里主要是因为后面要接着获取biz，需要使用cookie，而cookie可能在那个时候就已经失效了，这是需要重复执行当前循环
                if not is_html_obtain:
                    html = None
                    base_url = 'http://weixin.sogou.com/weixin?fr=sgsearch&ie=utf8&_asf=null&w=01029901&cid=null&query='
                    url = base_url + str(wx_name) + '&_ast=' + str(int(time.time()))
                    html = self.get_search_page(cookie, url, settings.max_retry)

                #判断search页面是否请求成功
                if html:
                    is_html_obtain = True
                    #分析html页面，获取文章href链接： href_ret_code:0代表找不到，1代表cookie失效，2代表成功
                    href_ret_code, href = self.get_href(html, wx_account)
                else:
                    logger.debug('SearchWorker:get_search_page failed:%s' % url)
                    wx_id = None
                    continue

                #进而获取biz：这个请求依赖于search页面是否请求成功
                if href_ret_code == 0:
                    logger.debug('SearchWorker:put into p_queue (%s, %s)' % (wx_id, 0))
                    self.p_queue.put((wx_id, 0))
                    wx_id, is_html_obtain = None, False
                elif href_ret_code == 1:
                    logger.debug('SearchWorker:cookie is invalid')
                    cookie = None
                    continue
                elif href_ret_code == 2:
                    article_url = 'http://weixin.sogou.com' + href
                    logger.debug('SearchWorker:get wx_biz start:%s' % article_url)
                    biz_ret_code, wx_biz = self.get_biz(article_url, cookie)
                    logger.debug('SearchWorker:get wx_biz end:(%s, %s)' % (biz_ret_code, wx_biz))
                    #如果获取wx_biz失败，则将该wx_biz的值设置成0
                    if wx_biz is None:
                        wx_biz = 0
                    logger.debug('SearchWorker:put into p_queue wx_id, wx_biz (%s, %s)' % (wx_id, wx_biz))
                    self.p_queue.put((wx_id, wx_biz))
                    wx_id, is_html_obtain = None, False
                else:
                    pass

            except Exception, e:
                logger.debug('SearchWorker:Exception %s' % e)

            # time.sleep(5)

    def get_search_page(self, cookie, url, max_retry):
        html = None
        i = 0
        while 1:
            try:
                opener = urllib2.build_opener()
                opener.addheaders.append(('Cookie', cookie))
                html = opener.open(url).read()
            except urllib2.HTTPError, e:
                logger.debug('SearchWorker:get_search_page:urllib2.HTTPError %s' % e)
                i += 1
                if i > max_retry:
                    break
                logger.debug('SearchWorker:get_search_page:Retry %s times' % i)
                continue
            except Exception, e:
                logger.debug('SearchWorker:get_search_page:Exception %s' % e)
            #除非发生urllib2.HTTPError异常可以重试，其他情况一律break
            break

        return html

    def get_biz(self, url, cookie):
        biz_ret_code, wx_biz = 0, None
        headers = {
            "Accept-Language": "zh-CN,zh;q=0.8",
            "Referer":"http://weixin.sogou.com",
            "Upgrade-Insecure-Requests": 1,
            "Connection": "keep-alive",
            "User-Agent":"Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.8.1.14) Gecko/20080404 (FoxPlus) Firefox/2.0.0.14",
            "Cookie":cookie
        }
        i = 0
        while 1:
            try:
                logger.debug('SearchWorker:get_biz:requests start:%s' % url)
                r = requests.get(url, allow_redirects=False, headers=headers)
                if r.status_code == 302:
                    location = r.headers.get('location', None)
                    logger.debug('SearchWorker:get_biz:location %s' % location)
                    if location:
                        wx_biz = urlparse.parse_qs(urlparse.urlparse(location).query)['__biz'][0]
                        biz_ret_code = 2
                    else:
                        biz_ret_code = 1
                        logger.debug('SearchWorker:get_biz:cookie is invalid %s' % url)
                        continue
                else:
                    logger.debug('SearchWorker:get_biz:requests failed:%s' % url)
                    biz_ret_code = 0
                break
            except requests.exceptions.RequestException as e:
                logger.debug('SearchWorker:get_biz:requests.exceptions.RequestException %s, %s' % (e, url))
                i+=1
                if i > settings.max_retry:
                    break
                logger.debug('SearchWorker:get_biz:Retry %s times:%s' % (i, url))
                continue
            except Exception as e:
                logger.debug('SearchWorker:get_biz:Exception %s' % e)

        return (biz_ret_code, wx_biz)

    def get_href(self, html, wx_account):
        #href_ret_code:0代表找不到，1代表cookie失效，2代表成功
        code_not_found, code_cookie_invalid, code_ok = 0, 1, 2
        href_ret_code, href = 0, None
        try:
            bs = BeautifulSoup(html, 'html.parser')

            # 如果出现了验证码的表单，则说明cookie已经无效
            if bs.select('#seccodeForm'):
                logger.debug('SearchWorker:get_href: seccodeForm page')
                return (code_cookie_invalid, None)
            
            #检测是否能搜到数据，如果搜不到，则返回
            wx_txt_boxes = bs.select('div.txt-box')
            if not wx_txt_boxes:
                logger.debug('SearchWorker:get_href:wx_txt_boxes len is 0')
                return (code_not_found, None)
            
            for box in wx_txt_boxes:
                accounts = box.select('h4 > span')
                articles = box.select('a.blue')
                #检测是否有account
                if not accounts:
                    continue
                account = accounts[0].get_text().split("：")[1].strip()
                if account != wx_account:
                    continue
                #检测是否有最新文章的链接，如果没有，则要写入日志，提示sogou上无法拿到此账号的biz
                if not articles:
                    logger.debug('SearchWorker:get_href:wx_account:%s have no latest articles')
                    continue
                href = articles[0].get('href')
                logger.debug('SearchWorker:get_href:href is %s' % href)
                break

            if not href:
                return (code_not_found,None)

            return (code_ok, href)
        except Exception as e:
            logger.debug('SearchWorker:get_href:Exception %s' % e)
            logger.debug('SearchWorker:get_href:html: %s' % html)
            logger.debug("SearchWorker:sleeping")
            time.sleep(10)
            return (code_not_found, None)
