# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
import MySQLdb
import time


class DbGetterManager(object):
    def __init__(self, p_queue, c_thread_num=1, p_thread_num=1):
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(DbGetterWorker(self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('DbGetterManager:Putting (None, None, None) into p_queue')
            self.p_queue.put((None, None, None))


class DbGetterWorker(threading.Thread):
    def __init__(self, p_queue):
        super(DbGetterWorker, self).__init__()
        self.p_queue = p_queue
        self.conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db,
                                    charset='utf8')
        self.cursor = self.conn.cursor()
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            try:
                logger.debug('DbGetterWorker:Getting data from db start')
                sql_str = """select wx_id, wx_name, wx_account from %s where wx_biz is null""" % settings.tb_weixin
                self.cursor.execute(sql_str)
                rows = self.cursor.fetchall()
                logger.debug('DbGetterWorker:Getting data from db end')

                for row in rows:
                    logger.debug('DbGetterWorker:Putting into q:(%s, %s, %s)' % row)
                    if self.p_queue.full():
                        time.sleep(5)
                    self.p_queue.put(row)
                break
            except MySQLdb.Error as e:
                logger.error('DbGetterWorker: MySQLdb.Error %s' % e)
            except Exception as e:
                logger.error('Exception in DbGetterWorker:%s' % e)

        logger.debug('DbGetterWorker:closing db conn, cursor start')
        self.cursor.close()
        self.conn.close()
        logger.debug('DbGetterWorker:closing db conn, cursor end')
