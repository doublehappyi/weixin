# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
import MySQLdb

class DbUpdaterManager(object):
    def __init__(self, c_queue, c_thread_num=1):
        self.c_queue = c_queue
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(DbUpdaterWorker(self.c_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()

class DbUpdaterWorker(threading.Thread):
    def __init__(self, c_queue):
        super(DbUpdaterWorker, self).__init__()
        self.c_queue = c_queue
        if settings.passwd:
            self.conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db, charset="utf8")
        else:
            self.conn = MySQLdb.connect(host=settings.host, user=settings.user, db=settings.db, charset="utf8")
        self.cursor = self.conn.cursor()
        self.setDaemon(True)
        self.start()

    def run(self):
        while 1:
            try:
                logger.debug("DbUpdaterWorker:Getting data from c_queue start")
                wx_id, wx_biz = self.c_queue.get()
                logger.debug("DbUpdaterWorker:Getting data from c_queue end (%s, %s)" % (wx_id, wx_biz))
                
                if wx_id:
                    logger.debug('DbUpdaterWorker:update to db start (%s, %s)' % (wx_id, wx_biz))
                    sql_str = """update %s set wx_biz="%s" where wx_id=%s""" % (settings.tb_weixin, wx_biz, wx_id)
                    self.cursor.execute(sql_str)
                    self.conn.commit()
                    logger.debug('DbUpdaterWorker:update to db end (%s, %s)' % (wx_id, wx_biz))
                else:
                    logger.debug('DbUpdaterWorker: no more data in c_queue')
                    break
            except MySQLdb.Error, e:
                logger.error('DbUpdaterWorker: MySQLdb.Error %s' % e)
            except Exception as e:
                logger.error('Exception in DbUpdaterWorker:%s' % e)

        logger.debug('closing db conn, cursor start')
        self.cursor.close()
        self.conn.close()
        logger.debug('closing db conn, cursor end')