# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import MySQLdb
import Queue
from sogou_cookies import sogou_cookies
from db_getter import DbGetterManager
from search_spider import SearchManager
from db_updater import DbUpdaterManager

# def init_db_getter_queue(p_thread_num):
#     q = Queue.Queue()
#     conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db,
#                                charset='utf8')
#     cursor = conn.cursor()
#     sql_str = """select wx_id, wx_name, wx_account from %s where wx_biz is null""" % settings.tb_weixin
#     try:
#         logger.debug('init_db_getter_queue:Getting data from db start')
#         cursor.execute(sql_str)
#         rows = cursor.fetchall()
#         logger.debug('init_db_getter_queue:Getting data from db end')
#
#         for row in rows:
#             logger.debug('init_db_getter_queue:Putting  (%s, %s, %s) into q start' % row)
#             q.put(row)
#             logger.debug('init_db_getter_queue:Putting  (%s, %s, %s) into q end' % row)
#         return q
#     except MySQLdb.Error, e:
#         logger.error('init_page_queue: MySQLdb.Error %s' % e)
#     except Exception as e:
#         logger.error('Exception in init_page_queue')
#         return None

def init_cookie_queue(p_thread_num):
    q = Queue.Queue()
    for ck in sogou_cookies:
        logger.debug('init_cookie_queue:put into cookie_queue:%s' % ck)
        q.put(ck)
    for i in range(p_thread_num):
        logger.debug('init_cookie_queue:put into cookie_queue None')
        q.put(None)
    return q

if __name__ == '__main__':
    db_getter_thread_num = 1
    search_thread_num = 4
    db_updater_thread_num = 1

    cookie_queue = init_cookie_queue(search_thread_num)
    # db_getter_queue = init_db_getter_queue(db_getter_thread_num)
    db_getter_queue = Queue.Queue(100)
    db_updater_queue = Queue.Queue()

    db_getter_mgr = DbGetterManager(p_queue=db_getter_queue, c_thread_num=db_getter_thread_num, p_thread_num=search_thread_num)

    search_mgr = SearchManager(c_queue=cookie_queue, db_getter_queue=db_getter_queue, p_queue=db_updater_queue, c_thread_num=search_thread_num, p_thread_num=db_updater_thread_num)

    db_updater_mgr = DbUpdaterManager(db_updater_queue, c_thread_num=1)

    db_getter_mgr.join()
    search_mgr.join()
    db_updater_mgr.join()

    logger.debug('main.py finished!')
