# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import openpyxl
import MySQLdb
import time

if __name__ == '__main__':
    if len(sys.argv) > 1:
        file_path = sys.argv[1]
    else:
        file_path = 'data/weixin.xlsx'
    logger.debug('file_path: {0}'.format(file_path))

    conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db,charset='utf8')
    cursor = conn.cursor()
    wb = openpyxl.load_workbook(file_path, use_iterators=True)
    first_sheet = wb.get_sheet_names()[0]
    ws = wb.get_sheet_by_name(first_sheet)

    i = 0
    for row in ws.iter_rows():
        # 去掉第一行
        if i == 0:
            i += 1
            continue
        wx_name = str(row[0].value).strip()
        wx_account = str(row[1].value).strip()
        if wx_name and wx_account:
            select_str = """select wx_id from {table} """.format(table=settings.tb_weixin) + """where wx_account=%s"""
            #先检索数据库是否已经有了wx_account
            try:
                cursor.execute(select_str, wx_account)
                row = cursor.fetchone()
                if row:
                    logger.debug('wx_account is already exists:{0}'.format(wx_account))
                    continue
            except Exception as e:
                logger.debug('select Exception :{0}'.format(e))

            sql_str = """insert into %s values """ % settings.tb_weixin + """(%s, %s, %s, %s, %s, %s, %s)"""
            add_time = int(time.time())
            update_time = add_time
            try:
                logger.debug(sql_str % (None, wx_name, wx_account, None, 0, add_time, update_time))
                cursor.execute(sql_str, (None, wx_name, wx_account, None, 0, add_time, update_time))
                conn.commit()
            except Exception as e:
                logger.warning('insert Exception :{0}'.format(e))

    cursor.close()
    conn.close()
    logger.debug('Finished!')
