# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import codecs
import MySQLdb
import time


if __name__ == '__main__':
    if len(sys.argv) > 2:
        file_path = sys.argv[1]
    else:
        file_path = 'data/weixin.txt'

    try:
        conn = MySQLdb.connect(host=settings.host, user=settings.user, passwd=settings.passwd, db=settings.db,
                               charset='utf8')
        cursor = conn.cursor()

        with codecs.open(file_path, 'r', 'utf-8') as f:
            while 1:
                line = f.readline()
                if line:
                    line_list = line.encode('utf-8').split()
                    if len(line_list) < 2:
                        continue
                    name, account = line_list[0], line_list[1]
                    if not cursor.execute("""select wx_id from weixin where wx_account=%s""", (account,)):
                        row = (name, account, int(time.time()))
                        logger.debug("""insert into %s values """ % settings.tb_weixin + """(null, %s, %s, null, 0, %s)""" % row)
                        sql_str = """insert into %s values """ % settings.tb_weixin + """(null, %s, %s, null, 0, %s)"""
                        cursor.execute(sql_str, row)
                        conn.commit()
                else:
                    logger.debug('End of file: %s' % file_path)
                    break

    finally:
        conn.close()
        cursor.close()

    logger.debug('Finished !')
