# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################
from openpyxl import Workbook
import time

wb = Workbook()
dest_file = 'data/'+time.strftime("%Y-%m-%d", time.localtime())+'.xlsx'
ws = wb.worksheets[0]
ws.title = 'Data'

ws.append(('标题（带链接）', '公众号名称','公众号账号',  '发布日期', '阅读量','赞数量'))


conn = settings.getConnection()
cursor = conn.cursor()

wx_select_str = """select wx_id, wx_name, wx_account from %s""" % settings.tb_weixin
cursor.execute(wx_select_str)
wx_rows = cursor.fetchall()

row = 2
for wx_row in wx_rows:
    wx_id, wx_name, wx_account = wx_row
    article_select_str = """select article_title, article_url, article_publish_time, article_read_num, article_like_num from %s where ref_wx_id=%s""" % (settings.tb_article, wx_id)
    cursor.execute(article_select_str)
    article_rows = cursor.fetchall()
    for article_row in article_rows:
        article_title, article_url, article_publish_time, article_read_num, article_like_num = article_row
        #格式化时间
        article_publish_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(article_publish_time)))

        ws.append((article_title, wx_name, wx_account, article_publish_time, article_read_num, article_like_num))
        ws.cell(row=row, column=1).hyperlink = article_url
        row+=1

wb.save(filename=dest_file)
print 'Finished OK'






