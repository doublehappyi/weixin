# coding:utf-8
__author__ = 'yishuangxi'
import sys, os

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import threading
import urlparse
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoSuchAttributeException


class SearchDriverManager(object):
    def __init__(self, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(SearchDriverWorker(self.c_queue, self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('SearchDriverManager:Putting (None, None) into p_queue')
            self.p_queue.put((None,))


class SearchDriverWorker(threading.Thread):
    def __init__(self, c_queue, p_queue):
        super(SearchDriverWorker, self).__init__()
        self.c_queue = c_queue
        self.p_queue = p_queue
        self.driver = webdriver.Chrome()
        self.setDaemon(True)
        self.start()

    def run(self):
        base_url = 'http://weixin.sogou.com/weixin?fr=sgsearch&ie=utf8&_asf=null&w=01029901&cid=null&query='
        while 1:
            try:
                logger.debug('SearchDriverWorker:Getting data from c_queue start')
                row = self.c_queue.get()
                self.c_queue.task_done()
                logger.debug('SearchDriverWorker:Getting data from c_queue end %s' % str(row))

                if row[0] is None:
                    logger.debug('SearchDriverWorker:no more data in c_queue')
                    break

                wx_id, wx_name, wx_account = row
                search_url = base_url + wx_name + '&_ast=' + str(int(time.time()))
                logger.debug('SearchDriverWorker:search_url:{0}'.format(search_url))
                self.get_search_page(search_url)
                logger.debug('SearchDriverWorker: get_article_href_qrcode start')
                href, wx_qrcode = self.get_article_href_qrcode(wx_account)
                logger.debug('SearchDriverWorker: get_article_href_qrcode end href:{0}'.format(href))

                if href:
                    wx_biz = self.get_wx_biz(href)
                    if not wx_biz:
                        wx_biz = 0
                        logger.debug('SearchDriverWorker: get_wx_biz failed')
                else:
                    logger.debug('SearchDriverWorker: href is not found: {wx_id}, {wx_name}'.format(wx_id=wx_id, wx_name=wx_name))
                    wx_biz = 0
                    # continue

                logger.debug('SearchDriverWorker: put (wx_id, wx_biz, wx_qrcode) into p_queue start ({0}, {1}, {2})'.format(wx_id, wx_biz, wx_qrcode))
                self.p_queue.put((wx_id, wx_biz, wx_qrcode))
                logger.debug('SearchDriverWorker: put (wx_id, wx_biz, wx_qrcode) into p_queue end ({0}, {1}, {2})'.format(wx_id, wx_biz, wx_qrcode))

            except Exception as e:
                logger.debug('SearchDriverWorker: Exception {0}'.format(e))
                pass

    def get_search_page(self, url):
        ret_code = 0
        try:
            self.driver.get(url)
            while 1:
                current_url = self.driver.current_url
                if 'antispider' in urlparse.urlparse(current_url).path:
                    logger.debug('SearchDriverWorker: get_search_page: waiting for verify-code input')
                    time.sleep(10)
                else:
                    self.driver.get(url)
                    ret_code = 1
                    break
        except Exception as e:
            ret_code = 0
            logger.debug('SearchDriverWorker: get_search_page: Exception: %s' % e)
        return ret_code

    def get_wx_el(self, wx_account_els, wx_account):
        ret_element = None
        try:
            for el in wx_account_els:
                account = el.find_element_by_css_selector('.s-p3 > .sp-txt > a').text.split('：')[1].strip()
                logger.debug('get_wx_el: text; {0}'.format(account))
                # account = el.text.split('：')[1].strip()
                # logger.debug('SearchDriverWorker: get_wx_el: account, wx_account: ({0},{1})'.format(account, wx_account))
                # if account == wx_account:
                #     logger.debug('SearchDriverWorker: get_wx_el: account == wx_account ({0},{1})'.format(account, wx_account))
                #     ret_element = el
                #     logger.debug('SearchDriverWorker: get_wx_el:ret_element{0}'.format(ret_element))
                #     break
        except Exception as e:
            logger.debug('SearchDriverWorker: get_wx_el:Exception {0}, {1}'.format(wx_account, e))

        return ret_element

    def get_wx_biz(self, href):
        wx_biz = None
        while 1:
            try:
                self.driver.get(href)
                article_url = self.driver.current_url
                logger.debug('SearchDriverWorker: get_wx_biz: article_url:{0}'.format(article_url))
                wx_biz = urlparse.parse_qs(urlparse.urlparse(article_url).query)['__biz'][0]
                logger.debug('SearchDriverWorker: get_wx_biz: wx_biz:{0}'.format(wx_biz))
                break
            except WebDriverException as e:
                logger.debug('SearchDriverWorker: get_wx_biz WebDriverException E:{0})'.format(e))
            except Exception as e:
                wx_biz = None
                logger.debug('SearchDriverWorker: get_wx_biz Exception:{0}'.format(e))
                break

        return wx_biz

    def get_article_href_qrcode(self, wx_account):
        href, qrcode = None, None
        try:
            wx_boxes = self.driver.find_elements_by_css_selector('div.wx-rb_v1')
            for wx_box in wx_boxes:
                if wx_box.find_element_by_css_selector('h4 > span').text.split('：')[1].strip() == wx_account:
                    try:
                        href = wx_box.find_element_by_css_selector('.s-p3 > .sp-txt > a').get_attribute('href')
                        logger.debug('SearchDriverWorker: get_article_href_qrcode: href: {0}'.format(href))
                        qrcode = wx_box.find_element_by_css_selector('.pos-box > img').get_attribute('src')
                        logger.debug('SearchDriverWorker: get_article_href_qrcode: qrcode: {0}'.format(qrcode))
                        break
                    except NoSuchElementException as e:
                        logger.warning('SearchDriverWorker: get_article_href_qrcode: get a NoSuchElementException: {0}, {1}'.format((wx_account, e)))
                    except NoSuchAttributeException as e:
                        logger.warning('SearchDriverWorker: get_article_href_qrcode: get a NoSuchAttributeException: {0}, {1}'.format((wx_account, e)))
                    except Exception as e:
                        logger.warning('SearchDriverWorker: get_article_href_qrcode: get a Exception: {0}, {1}'.format((wx_account, e)))
                        break
        except IndexError as e:
            logger.warning('SearchDriverWorker: get_article_href_qrcode: IndexError: {0}, {1}'.format((wx_account, e)))
        except NoSuchElementException as e:
            logger.warning('SearchDriverWorker: get_article_href_qrcode: NoSuchElementException: {0}, {1}'.format((wx_account, e)))
        except Exception as e:
            logger.warning('SearchDriverWorker: get_article_href_qrcode: Exception: {0}, {1}'.format((wx_account, e)))

        return (href, qrcode)