﻿# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################

import Queue
from db_getter import DbGetterManager
from search_driver import SearchDriverManager
from db_updater import DbUpdaterManager


if __name__ == '__main__':
    db_getter_thread_num = 1
    search_thread_num = 20
    db_updater_thread_num = 1
    db_getter_queue = Queue.Queue(40)
    db_updater_queue = Queue.Queue()

    db_getter_mgr = DbGetterManager(p_queue=db_getter_queue, c_thread_num=db_getter_thread_num, p_thread_num=search_thread_num)

    search_mgr = SearchDriverManager(c_queue=db_getter_queue, p_queue=db_updater_queue, c_thread_num=search_thread_num, p_thread_num=db_updater_thread_num)

    db_updater_mgr = DbUpdaterManager(db_updater_queue, c_thread_num=db_updater_thread_num)

    db_getter_mgr.join()
    search_mgr.join()
    db_updater_mgr.join()

    logger.debug('main.py finished!')
