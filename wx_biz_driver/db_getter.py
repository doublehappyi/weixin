# coding:utf-8
__author__ = 'yishuangxi'
import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
#####################日志设置#####################
log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = settings.get_logger(log_path)
#####################日志设置#####################
import MySQLdb
import threading
import time


class DbGetterManager(object):
    def __init__(self, p_queue, c_thread_num=1, p_thread_num=1):
        self.p_queue = p_queue
        self.p_thread_num = p_thread_num
        self.threads = []
        self.init_threads(c_thread_num)

    def init_threads(self, c_thread_num):
        for i in xrange(c_thread_num):
            self.threads.append(DbGetterWorker(self.p_queue))

    def join(self):
        for t in self.threads:
            if t.isAlive():
                t.join()
        for i in xrange(self.p_thread_num):
            logger.debug('DbGetterManager:Putting (None, ) into p_queue')
            self.p_queue.put((None,))


class DbGetterWorker(threading.Thread):
    def __init__(self, p_queue):
        super(DbGetterWorker, self).__init__()
        self.p_queue = p_queue
        self.conn = settings.getConnection()
        self.cursor = self.conn.cursor()
        self.setDaemon(True)
        self.start()

    def run(self):
        try:
            logger.debug('DbGetterWorker:Getting data from db start')
            sql_str = """select wx_id, wx_name, wx_account from {table} where wx_biz is null""".format(table=settings.db['table']['weixin'])
            logger.debug('DbGetterWorker: sql_str: '+sql_str)
            self.cursor.execute(sql_str)
            rows = self.cursor.fetchall()
            # logger.debug('DbGetterWorker:Getting data from db end:{0}'.format(str(rows)))

            for row in rows:
                logger.debug('DbGetterWorker:Putting into q:{row}'.format( row=str(row) ))
                if self.p_queue.full():
                    time.sleep(5)
                self.p_queue.put(row)
        except MySQLdb.Error as e:
            logger.error('DbGetterWorker: MySQLdb.Error %s' % e)
        except Exception as e:
            logger.error('DbGetterWorker: Exception:%s' % e)

        logger.debug('DbGetterWorker:closing db conn, cursor start')
        self.cursor.close()
        self.conn.close()
        logger.debug('DbGetterWorker:closing db conn, cursor end')
